#!/bin/bash

##### very important before starting ####
# control if the key ID of GNU parellel was erased if the script did not finish without an error
# e.g. --id merge_rep_20150609  
# see folder in the home : .parallel


# Parse input, output folders from command line.
path_input=$1
path_output=$2

# Load modules
module load samtools/1.2
module load parallel/20140922
module load bedtools/2.17.0

# Determine the names of all factors in the input directory. Expected file name
# format is SomePrefix_FactorName_RepX.bam
names=$(find ${path_input}/*.bam  | sed "s#${path_input}##g" | cut -d_ -f2 | sort -u )

# Generate a token for this run.
token=`date '+%Y%m%d%H%M%S'`

# Loop over the factors, then over each replicate, then sort them all concurrently.
for name in $names
do
    all_bams=${path_input}/*${name}*.bam
    
    # Sorting will improve merge performance, and will avoid errors related
    # to missing headers in certain cases.
    for bam in $all_bams
    do
        bn=`basename $bam`
        sem --no-notice -j 10 --id $token.sort   samtools sort -f $bam ${path_output}/$bn.sorted
    done
done
sem --no-notice --wait --id $token.sort 

# Merge all files concurrently.
for name in $names
do
    all_bams=${path_output}/*${name}*.bam.sorted
    sem --no-notice -j 10 --id $token.merge   samtools merge ${path_output}/$name.bam $all_bams
done
sem --no-notice --wait --id $token.merge

# Remove sorted bams to free disk space.
rm ${path_output}/*.bam.sorted

# Index merged files.
for f in ${path_output}/*.bam
do
	sem --no-notice -j 10 --id $token.index samtools index $f
done
sem --no-notice --wait --id $token.index

#############
# variables #
#############
wig2BigWig=/is1/projects/SB_NGS/scripts/wigToBigWig
slopadjust=225

# Build a genomic reference from the bam headers, removing unmapped
# chromosomes and mitochondrial chromosomes.
# Will only work for humans.
samtools view -H ${path_output}/*.bam |
    grep -v 'GL000' |
    grep -v 'SN:MT' |
    grep -v '_random' |
    awk '$1 == "@SQ" {OFS="\t";print $2,$3}' - |
    sed 's/.N://g' > ${path_output}/genome

# Generate bigwig tracks.
for i in ${path_output}/*.bam
do
    # Calculate scale factor from mapped, high quality reads.
	count=$(samtools view -F516 -c $i)
	ScaleFactor=$(echo "1000000/${count}" | bc -l)

    # Generate coverage bigwigs.
    samtools view -b -F516 $i \
		| bedtools bamtobed \
		| bedtools slop -l 0 -r $slopadjust -s -i stdin -g ${path_output}/genome \
		| bedtools genomecov -i stdin -scale $ScaleFactor -g ${path_output}/genome -bg \
		| $wig2BigWig stdin ${path_output}/genome ${i%.bam}.rpm.bw
done

# Remove the merged bams
rm ${path_output}/*.bam
rm ${path_output}/*.bai
