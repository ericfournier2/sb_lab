export RAP_ID=fhq-091-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/output-DNAseq
$MUGQIC_PIPELINES_HOME/pipelines/dnaseq/dnaseq.py -s '1-28' -l debug \
    -r $PROJECT_BASE/input/config/dna-seq/readset.txt \
    -o $SCRATCH/projet_prostate/output-DNAseq \
    --config $MUGQIC_PIPELINES_HOME/pipelines/dnaseq/dnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/dnaseq/dnaseq.guillimin.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Homo_sapiens.GRCh38.ini
