export RAP_ID=fhq-091-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/output-ERCC
$MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.py -s '1-22' -l debug \
    -r $PROJECT_BASE/input/readset.txt \
    -d $PROJECT_BASE/input/design.txt \
    -o $SCRATCH/projet_prostate/output-ERCC \
    --config $PROJECT_BASE/input/config/rna-seq/rnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
        $PROJECT_BASE/input/config/rna-seq/Homo_sapiens.GRCh38.ERCC.ini \
        $PROJECT_BASE/input/config/rna-seq/this_run.ini