#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# ChipSeq PBSScheduler Job Submission Bash script
# Version: 2.2.0
# Created on: 2016-05-09T12:09:43
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 10 jobs
#   merge_trimmomatic_stats: 1 job
#   bwa_mem_picard_sort_sam: 11 jobs
#   samtools_view_filter: 11 jobs
#   picard_merge_sam_files: 10 jobs
#   picard_mark_duplicates: 11 jobs
#   metrics: 2 jobs
#   homer_make_tag_directory: 10 jobs
#   qc_metrics: 1 job
#   homer_make_ucsc_file: 11 jobs
#   macs2_callpeak: 6 jobs
#   homer_annotate_peaks: 6 jobs
#   homer_find_motifs_genome: 1 job
#   annotation_graphs: 1 job
#   TOTAL: 92 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/output/Prostate
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/ChipSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR


#-------------------------------------------------------------------------------
# STEP: trimmomatic
#-------------------------------------------------------------------------------
STEP=trimmomatic
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: trimmomatic_1_JOB_ID: trimmomatic.RS_H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_H3K27Ac-DHT
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_H3K27Ac-DHT.09e1bb7028ff71dc6798a18cbbdf8e1c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_H3K27Ac-DHT.09e1bb7028ff71dc6798a18cbbdf8e1c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/H3K27Ac-DHT && \
`cat > trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1015787.fastq \
  trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.single.fastq.gz \
  ILLUMINACLIP:trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.log
trimmomatic.RS_H3K27Ac-DHT.09e1bb7028ff71dc6798a18cbbdf8e1c.mugqic.done
)
trimmomatic_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_2_JOB_ID: trimmomatic.RS_H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_H3K27AcWithDHT
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_H3K27AcWithDHT.d76e415b86c137cf0fa0b018f3c55ac1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_H3K27AcWithDHT.d76e415b86c137cf0fa0b018f3c55ac1.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/H3K27AcWithDHT && \
`cat > trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1015788.fastq \
  trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.single.fastq.gz \
  ILLUMINACLIP:trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.log
trimmomatic.RS_H3K27AcWithDHT.d76e415b86c137cf0fa0b018f3c55ac1.mugqic.done
)
trimmomatic_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_3_JOB_ID: trimmomatic.RS_InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_InputDNA1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_InputDNA1.d66f3bf6b9dabb09b590fc27222ecfeb.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_InputDNA1.d66f3bf6b9dabb09b590fc27222ecfeb.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/InputDNA1 && \
`cat > trim/InputDNA1/RS_InputDNA1.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1015790.fastq \
  trim/InputDNA1/RS_InputDNA1.trim.single.fastq.gz \
  ILLUMINACLIP:trim/InputDNA1/RS_InputDNA1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/InputDNA1/RS_InputDNA1.trim.log
trimmomatic.RS_InputDNA1.d66f3bf6b9dabb09b590fc27222ecfeb.mugqic.done
)
trimmomatic_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_4_JOB_ID: trimmomatic.RS_InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_InputDNA2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_InputDNA2.d7a802f744ba8acdd321e09dd75c20f7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_InputDNA2.d7a802f744ba8acdd321e09dd75c20f7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/InputDNA2 && \
`cat > trim/InputDNA2/RS_InputDNA2.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1015791.fastq \
  trim/InputDNA2/RS_InputDNA2.trim.single.fastq.gz \
  ILLUMINACLIP:trim/InputDNA2/RS_InputDNA2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/InputDNA2/RS_InputDNA2.trim.log
trimmomatic.RS_InputDNA2.d7a802f744ba8acdd321e09dd75c20f7.mugqic.done
)
trimmomatic_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_5_JOB_ID: trimmomatic.RS_LNCaP-H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_LNCaP-H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_LNCaP-H3K27ac-dht-siCTRL.830b81c5a085551ba3f73611c522edb7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_LNCaP-H3K27ac-dht-siCTRL.830b81c5a085551ba3f73611c522edb7.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/H3K27ac-dht-siCTRL && \
`cat > trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1170737.fastq \
  trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.single.fastq.gz \
  ILLUMINACLIP:trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.log
trimmomatic.RS_LNCaP-H3K27ac-dht-siCTRL.830b81c5a085551ba3f73611c522edb7.mugqic.done
)
trimmomatic_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_6_JOB_ID: trimmomatic.RS_LNCaP-H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_LNCaP-H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_LNCaP-H3K27ac-dht-siFoxA1.19b593099a901a82c772271c70f54bd2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_LNCaP-H3K27ac-dht-siFoxA1.19b593099a901a82c772271c70f54bd2.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/H3K27ac-dht-siFoxA1 && \
`cat > trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR1170738.fastq \
  trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.single.fastq.gz \
  ILLUMINACLIP:trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.log
trimmomatic.RS_LNCaP-H3K27ac-dht-siFoxA1.19b593099a901a82c772271c70f54bd2.mugqic.done
)
trimmomatic_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_7_JOB_ID: trimmomatic.RS_LNCaP-input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_LNCaP-input-dht1
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_LNCaP-input-dht1.ea7f8b65c4bf3de2ad078dc553a921f9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_LNCaP-input-dht1.ea7f8b65c4bf3de2ad078dc553a921f9.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/Input-dht1 && \
`cat > trim/Input-dht1/RS_LNCaP-input-dht1.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR122327.fastq \
  trim/Input-dht1/RS_LNCaP-input-dht1.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Input-dht1/RS_LNCaP-input-dht1.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/Input-dht1/RS_LNCaP-input-dht1.trim.log
trimmomatic.RS_LNCaP-input-dht1.ea7f8b65c4bf3de2ad078dc553a921f9.mugqic.done
)
trimmomatic_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_8_JOB_ID: trimmomatic.RS_LNCaP-input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_LNCaP-input-dht2
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_LNCaP-input-dht2.f5a7b9a1782440038ac5a32264b27898.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_LNCaP-input-dht2.f5a7b9a1782440038ac5a32264b27898.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/Input-dht2 && \
`cat > trim/Input-dht2/RS_LNCaP-input-dht2.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR122328.fastq \
  trim/Input-dht2/RS_LNCaP-input-dht2.trim.single.fastq.gz \
  ILLUMINACLIP:trim/Input-dht2/RS_LNCaP-input-dht2.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/Input-dht2/RS_LNCaP-input-dht2.trim.log
trimmomatic.RS_LNCaP-input-dht2.f5a7b9a1782440038ac5a32264b27898.mugqic.done
)
trimmomatic_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_9_JOB_ID: trimmomatic.RS_H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_H3K27ac
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_H3K27ac.dd86da22b86d2ad3ec40a5ea4720c9bc.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_H3K27ac.dd86da22b86d2ad3ec40a5ea4720c9bc.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/H3K27ac && \
`cat > trim/H3K27ac/RS_H3K27ac.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR122337.fastq \
  trim/H3K27ac/RS_H3K27ac.trim.single.fastq.gz \
  ILLUMINACLIP:trim/H3K27ac/RS_H3K27ac.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/H3K27ac/RS_H3K27ac.trim.log
trimmomatic.RS_H3K27ac.dd86da22b86d2ad3ec40a5ea4720c9bc.mugqic.done
)
trimmomatic_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: trimmomatic_10_JOB_ID: trimmomatic.RS_IgG
#-------------------------------------------------------------------------------
JOB_NAME=trimmomatic.RS_IgG
JOB_DEPENDENCIES=
JOB_DONE=job_output/trimmomatic/trimmomatic.RS_IgG.c730960a096421b05505465aa2c3d418.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'trimmomatic.RS_IgG.c730960a096421b05505465aa2c3d418.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/trimmomatic/0.35 && \
mkdir -p trim/IgG && \
`cat > trim/IgG/RS_IgG.trim.adapters.fa << END
>Single
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
END
` && \
java -XX:ParallelGCThreads=1 -Xmx2G -jar $TRIMMOMATIC_JAR SE \
  -threads 20 \
  -phred33 \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/SRR122338.fastq \
  trim/IgG/RS_IgG.trim.single.fastq.gz \
  ILLUMINACLIP:trim/IgG/RS_IgG.trim.adapters.fa:2:30:15 \
  TRAILING:30 \
  MINLEN:20 \
  2> trim/IgG/RS_IgG.trim.log
trimmomatic.RS_IgG.c730960a096421b05505465aa2c3d418.mugqic.done
)
trimmomatic_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$trimmomatic_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
STEP=merge_trimmomatic_stats
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: merge_trimmomatic_stats_1_JOB_ID: merge_trimmomatic_stats
#-------------------------------------------------------------------------------
JOB_NAME=merge_trimmomatic_stats
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID:$trimmomatic_2_JOB_ID:$trimmomatic_3_JOB_ID:$trimmomatic_4_JOB_ID:$trimmomatic_5_JOB_ID:$trimmomatic_6_JOB_ID:$trimmomatic_7_JOB_ID:$trimmomatic_8_JOB_ID:$trimmomatic_9_JOB_ID:$trimmomatic_10_JOB_ID
JOB_DONE=job_output/merge_trimmomatic_stats/merge_trimmomatic_stats.2829031049d0d5ddcb1235c5ce88af78.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'merge_trimmomatic_stats.2829031049d0d5ddcb1235c5ce88af78.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p metrics && \
echo 'Sample	Readset	Raw Single Reads #	Surviving Single Reads #	Surviving Single Reads %' > metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K27Ac-DHT	RS_H3K27Ac-DHT	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K27AcWithDHT	RS_H3K27AcWithDHT	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/InputDNA1/RS_InputDNA1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/InputDNA1	RS_InputDNA1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/InputDNA2/RS_InputDNA2.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/InputDNA2	RS_InputDNA2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K27ac-dht-siCTRL	RS_LNCaP-H3K27ac-dht-siCTRL	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K27ac-dht-siFoxA1	RS_LNCaP-H3K27ac-dht-siFoxA1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Input-dht1/RS_LNCaP-input-dht1.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Input-dht1	RS_LNCaP-input-dht1	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/Input-dht2/RS_LNCaP-input-dht2.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/Input-dht2	RS_LNCaP-input-dht2	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/H3K27ac/RS_H3K27ac.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/H3K27ac	RS_H3K27ac	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
grep ^Input trim/IgG/RS_IgG.trim.log | \
perl -pe 's/^Input Reads: (\d+).*Surviving: (\d+).*$/IgG	RS_IgG	\1	\2/' | \
awk '{OFS="	"; print $0, $4 / $3 * 100}' \
  >> metrics/trimReadsetTable.tsv && \
cut -f1,3- metrics/trimReadsetTable.tsv | awk -F"	" '{OFS="	"; if (NR==1) {if ($2=="Raw Paired Reads #") {paired=1};print "Sample", "Raw Reads #", "Surviving Reads #", "Surviving %"} else {if (paired) {$2=$2*2; $3=$3*2}; raw[$1]+=$2; surviving[$1]+=$3}}END{for (sample in raw){print sample, raw[sample], surviving[sample], surviving[sample] / raw[sample] * 100}}' \
  > metrics/trimSampleTable.tsv && \
mkdir -p report && \
cp metrics/trimReadsetTable.tsv metrics/trimSampleTable.tsv report/ && \
trim_readset_table_md=`LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:"} else {print $1, $2, sprintf("%\47d", $3), sprintf("%\47d", $4), sprintf("%.1f", $5)}}' metrics/trimReadsetTable.tsv` && \
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/Illumina.merge_trimmomatic_stats.md \
  --variable trailing_min_quality=30 \
  --variable min_length=20 \
  --variable read_type=Single \
  --variable trim_readset_table="$trim_readset_table_md" \
  --to markdown \
  > report/Illumina.merge_trimmomatic_stats.md
merge_trimmomatic_stats.2829031049d0d5ddcb1235c5ce88af78.mugqic.done
)
merge_trimmomatic_stats_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$merge_trimmomatic_stats_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: bwa_mem_picard_sort_sam
#-------------------------------------------------------------------------------
STEP=bwa_mem_picard_sort_sam
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_1_JOB_ID: bwa_mem_picard_sort_sam.RS_H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_H3K27Ac-DHT
JOB_DEPENDENCIES=$trimmomatic_1_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_H3K27Ac-DHT.447575bfef7cfbe796e861cc10663c95.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_H3K27Ac-DHT.447575bfef7cfbe796e861cc10663c95.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/H3K27Ac-DHT/RS_H3K27Ac-DHT && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_H3K27Ac-DHT	SM:H3K27Ac-DHT	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/H3K27Ac-DHT/RS_H3K27Ac-DHT.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/H3K27Ac-DHT/RS_H3K27Ac-DHT/RS_H3K27Ac-DHT.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_H3K27Ac-DHT.447575bfef7cfbe796e861cc10663c95.mugqic.done
)
bwa_mem_picard_sort_sam_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_2_JOB_ID: bwa_mem_picard_sort_sam.RS_H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_H3K27AcWithDHT
JOB_DEPENDENCIES=$trimmomatic_2_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_H3K27AcWithDHT.d9249dcde4dfdcaf3b0cae7052697074.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_H3K27AcWithDHT.d9249dcde4dfdcaf3b0cae7052697074.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/H3K27AcWithDHT/RS_H3K27AcWithDHT && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_H3K27AcWithDHT	SM:H3K27AcWithDHT	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/H3K27AcWithDHT/RS_H3K27AcWithDHT.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/H3K27AcWithDHT/RS_H3K27AcWithDHT/RS_H3K27AcWithDHT.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_H3K27AcWithDHT.d9249dcde4dfdcaf3b0cae7052697074.mugqic.done
)
bwa_mem_picard_sort_sam_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_3_JOB_ID: bwa_mem_picard_sort_sam.RS_InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_InputDNA1
JOB_DEPENDENCIES=$trimmomatic_3_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_InputDNA1.bc4795b2dd7c6f88320c0276f61afa55.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_InputDNA1.bc4795b2dd7c6f88320c0276f61afa55.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/InputDNA1/RS_InputDNA1 && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_InputDNA1	SM:InputDNA1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/InputDNA1/RS_InputDNA1.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/InputDNA1/RS_InputDNA1/RS_InputDNA1.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_InputDNA1.bc4795b2dd7c6f88320c0276f61afa55.mugqic.done
)
bwa_mem_picard_sort_sam_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_4_JOB_ID: bwa_mem_picard_sort_sam.RS_InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_InputDNA2
JOB_DEPENDENCIES=$trimmomatic_4_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_InputDNA2.0cb3894f1b7cfeca7af8b368682f051b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_InputDNA2.0cb3894f1b7cfeca7af8b368682f051b.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/InputDNA2/RS_InputDNA2 && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_InputDNA2	SM:InputDNA2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/InputDNA2/RS_InputDNA2.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/InputDNA2/RS_InputDNA2/RS_InputDNA2.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_InputDNA2.0cb3894f1b7cfeca7af8b368682f051b.mugqic.done
)
bwa_mem_picard_sort_sam_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_5_JOB_ID: bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$trimmomatic_5_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siCTRL.364b3552fefa8f1df3fe955afec0cbf9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siCTRL.364b3552fefa8f1df3fe955afec0cbf9.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_LNCaP-H3K27ac-dht-siCTRL	SM:H3K27ac-dht-siCTRL	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siCTRL.364b3552fefa8f1df3fe955afec0cbf9.mugqic.done
)
bwa_mem_picard_sort_sam_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_6_JOB_ID: bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$trimmomatic_6_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siFoxA1.fc50c1bb10001f72601d7720845a6152.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siFoxA1.fc50c1bb10001f72601d7720845a6152.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1 && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_LNCaP-H3K27ac-dht-siFoxA1	SM:H3K27ac-dht-siFoxA1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_LNCaP-H3K27ac-dht-siFoxA1.fc50c1bb10001f72601d7720845a6152.mugqic.done
)
bwa_mem_picard_sort_sam_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_7_JOB_ID: bwa_mem_picard_sort_sam.RS_LNCaP-input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_LNCaP-input-dht1
JOB_DEPENDENCIES=$trimmomatic_7_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_LNCaP-input-dht1.6699916ce504efa1a4adfac51c51d805.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_LNCaP-input-dht1.6699916ce504efa1a4adfac51c51d805.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/Input-dht1/RS_LNCaP-input-dht1 && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_LNCaP-input-dht1	SM:Input-dht1	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/Input-dht1/RS_LNCaP-input-dht1.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/Input-dht1/RS_LNCaP-input-dht1/RS_LNCaP-input-dht1.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_LNCaP-input-dht1.6699916ce504efa1a4adfac51c51d805.mugqic.done
)
bwa_mem_picard_sort_sam_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_8_JOB_ID: bwa_mem_picard_sort_sam.RS_LNCaP-input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_LNCaP-input-dht2
JOB_DEPENDENCIES=$trimmomatic_8_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_LNCaP-input-dht2.2ee586e6023fb2cda65e0e2f6eff5a64.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_LNCaP-input-dht2.2ee586e6023fb2cda65e0e2f6eff5a64.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/Input-dht2/RS_LNCaP-input-dht2 && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_LNCaP-input-dht2	SM:Input-dht2	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/Input-dht2/RS_LNCaP-input-dht2.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/Input-dht2/RS_LNCaP-input-dht2/RS_LNCaP-input-dht2.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_LNCaP-input-dht2.2ee586e6023fb2cda65e0e2f6eff5a64.mugqic.done
)
bwa_mem_picard_sort_sam_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_9_JOB_ID: bwa_mem_picard_sort_sam.RS_H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_H3K27ac
JOB_DEPENDENCIES=$trimmomatic_9_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_H3K27ac.30c034cd3b8cf69d14413c4e953357b2.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_H3K27ac.30c034cd3b8cf69d14413c4e953357b2.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/H3K27ac/RS_H3K27ac && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_H3K27ac	SM:H3K27ac	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/H3K27ac/RS_H3K27ac.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/H3K27ac/RS_H3K27ac/RS_H3K27ac.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_H3K27ac.30c034cd3b8cf69d14413c4e953357b2.mugqic.done
)
bwa_mem_picard_sort_sam_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_10_JOB_ID: bwa_mem_picard_sort_sam.RS_IgG
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam.RS_IgG
JOB_DEPENDENCIES=$trimmomatic_10_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam.RS_IgG.8c8cdbfef6dbbe9c16297be178cfa720.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam.RS_IgG.8c8cdbfef6dbbe9c16297be178cfa720.mugqic.done'
module load mugqic/bwa/0.7.12 mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
mkdir -p alignment/IgG/RS_IgG && \
bwa mem  \
  -M -t 20 \
  -R '@RG	ID:RS_IgG	SM:IgG	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.hg19/genome/bwa_index/Homo_sapiens.hg19.fa \
  trim/IgG/RS_IgG.trim.single.fastq.gz | \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=/dev/stdin \
  OUTPUT=alignment/IgG/RS_IgG/RS_IgG.sorted.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=3750000
bwa_mem_picard_sort_sam.RS_IgG.8c8cdbfef6dbbe9c16297be178cfa720.mugqic.done
)
bwa_mem_picard_sort_sam_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: bwa_mem_picard_sort_sam_11_JOB_ID: bwa_mem_picard_sort_sam_report
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_picard_sort_sam_report
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID:$bwa_mem_picard_sort_sam_2_JOB_ID:$bwa_mem_picard_sort_sam_3_JOB_ID:$bwa_mem_picard_sort_sam_4_JOB_ID:$bwa_mem_picard_sort_sam_5_JOB_ID:$bwa_mem_picard_sort_sam_6_JOB_ID:$bwa_mem_picard_sort_sam_7_JOB_ID:$bwa_mem_picard_sort_sam_8_JOB_ID:$bwa_mem_picard_sort_sam_9_JOB_ID:$bwa_mem_picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/bwa_mem_picard_sort_sam/bwa_mem_picard_sort_sam_report.ccf108459067256486a8f50c1a551880.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_picard_sort_sam_report.ccf108459067256486a8f50c1a551880.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  --variable scientific_name="Homo_sapiens" \
  --variable assembly="hg19" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/DnaSeq.bwa_mem_picard_sort_sam.md \
  > report/DnaSeq.bwa_mem_picard_sort_sam.md
bwa_mem_picard_sort_sam_report.ccf108459067256486a8f50c1a551880.mugqic.done
)
bwa_mem_picard_sort_sam_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$bwa_mem_picard_sort_sam_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: samtools_view_filter
#-------------------------------------------------------------------------------
STEP=samtools_view_filter
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_1_JOB_ID: samtools_view_filter.RS_H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_H3K27Ac-DHT
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_1_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_H3K27Ac-DHT.8978cb00a5854a84652c1dc095e0f52b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_H3K27Ac-DHT.8978cb00a5854a84652c1dc095e0f52b.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/H3K27Ac-DHT/RS_H3K27Ac-DHT/RS_H3K27Ac-DHT.sorted.bam \
  > alignment/H3K27Ac-DHT/RS_H3K27Ac-DHT/RS_H3K27Ac-DHT.sorted.filtered.bam
samtools_view_filter.RS_H3K27Ac-DHT.8978cb00a5854a84652c1dc095e0f52b.mugqic.done
)
samtools_view_filter_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_2_JOB_ID: samtools_view_filter.RS_H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_H3K27AcWithDHT
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_2_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_H3K27AcWithDHT.a129bdbaeba16396b36d1496d03a3c1b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_H3K27AcWithDHT.a129bdbaeba16396b36d1496d03a3c1b.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/H3K27AcWithDHT/RS_H3K27AcWithDHT/RS_H3K27AcWithDHT.sorted.bam \
  > alignment/H3K27AcWithDHT/RS_H3K27AcWithDHT/RS_H3K27AcWithDHT.sorted.filtered.bam
samtools_view_filter.RS_H3K27AcWithDHT.a129bdbaeba16396b36d1496d03a3c1b.mugqic.done
)
samtools_view_filter_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_3_JOB_ID: samtools_view_filter.RS_InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_InputDNA1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_3_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_InputDNA1.d8b9e5ae0f879cdf1df109f81d8564ca.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_InputDNA1.d8b9e5ae0f879cdf1df109f81d8564ca.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/InputDNA1/RS_InputDNA1/RS_InputDNA1.sorted.bam \
  > alignment/InputDNA1/RS_InputDNA1/RS_InputDNA1.sorted.filtered.bam
samtools_view_filter.RS_InputDNA1.d8b9e5ae0f879cdf1df109f81d8564ca.mugqic.done
)
samtools_view_filter_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_4_JOB_ID: samtools_view_filter.RS_InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_InputDNA2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_4_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_InputDNA2.bb1fa1e6326abda658a09245412d3550.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_InputDNA2.bb1fa1e6326abda658a09245412d3550.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/InputDNA2/RS_InputDNA2/RS_InputDNA2.sorted.bam \
  > alignment/InputDNA2/RS_InputDNA2/RS_InputDNA2.sorted.filtered.bam
samtools_view_filter.RS_InputDNA2.bb1fa1e6326abda658a09245412d3550.mugqic.done
)
samtools_view_filter_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_5_JOB_ID: samtools_view_filter.RS_LNCaP-H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_LNCaP-H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_5_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_LNCaP-H3K27ac-dht-siCTRL.8c9caf99f53a46a2bc800acff385e49d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_LNCaP-H3K27ac-dht-siCTRL.8c9caf99f53a46a2bc800acff385e49d.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.sorted.bam \
  > alignment/H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.sorted.filtered.bam
samtools_view_filter.RS_LNCaP-H3K27ac-dht-siCTRL.8c9caf99f53a46a2bc800acff385e49d.mugqic.done
)
samtools_view_filter_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_6_JOB_ID: samtools_view_filter.RS_LNCaP-H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_LNCaP-H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_6_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_LNCaP-H3K27ac-dht-siFoxA1.288bd097c6edcf6eba32123b2ecba877.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_LNCaP-H3K27ac-dht-siFoxA1.288bd097c6edcf6eba32123b2ecba877.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.sorted.bam \
  > alignment/H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.sorted.filtered.bam
samtools_view_filter.RS_LNCaP-H3K27ac-dht-siFoxA1.288bd097c6edcf6eba32123b2ecba877.mugqic.done
)
samtools_view_filter_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_7_JOB_ID: samtools_view_filter.RS_LNCaP-input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_LNCaP-input-dht1
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_7_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_LNCaP-input-dht1.3dc8b77e1465c7cc73c43a57a115dc78.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_LNCaP-input-dht1.3dc8b77e1465c7cc73c43a57a115dc78.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/Input-dht1/RS_LNCaP-input-dht1/RS_LNCaP-input-dht1.sorted.bam \
  > alignment/Input-dht1/RS_LNCaP-input-dht1/RS_LNCaP-input-dht1.sorted.filtered.bam
samtools_view_filter.RS_LNCaP-input-dht1.3dc8b77e1465c7cc73c43a57a115dc78.mugqic.done
)
samtools_view_filter_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_8_JOB_ID: samtools_view_filter.RS_LNCaP-input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_LNCaP-input-dht2
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_8_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_LNCaP-input-dht2.6f2e4c346e601f5959bddb017b6f353b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_LNCaP-input-dht2.6f2e4c346e601f5959bddb017b6f353b.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/Input-dht2/RS_LNCaP-input-dht2/RS_LNCaP-input-dht2.sorted.bam \
  > alignment/Input-dht2/RS_LNCaP-input-dht2/RS_LNCaP-input-dht2.sorted.filtered.bam
samtools_view_filter.RS_LNCaP-input-dht2.6f2e4c346e601f5959bddb017b6f353b.mugqic.done
)
samtools_view_filter_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_9_JOB_ID: samtools_view_filter.RS_H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_H3K27ac
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_9_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_H3K27ac.f65b4913f66771292aff44d33b90095d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_H3K27ac.f65b4913f66771292aff44d33b90095d.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/H3K27ac/RS_H3K27ac/RS_H3K27ac.sorted.bam \
  > alignment/H3K27ac/RS_H3K27ac/RS_H3K27ac.sorted.filtered.bam
samtools_view_filter.RS_H3K27ac.f65b4913f66771292aff44d33b90095d.mugqic.done
)
samtools_view_filter_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_10_JOB_ID: samtools_view_filter.RS_IgG
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter.RS_IgG
JOB_DEPENDENCIES=$bwa_mem_picard_sort_sam_10_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter.RS_IgG.cd6927b194a618a35ce01622c6d58a37.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter.RS_IgG.cd6927b194a618a35ce01622c6d58a37.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools view -b -F4 -q 20 \
  alignment/IgG/RS_IgG/RS_IgG.sorted.bam \
  > alignment/IgG/RS_IgG/RS_IgG.sorted.filtered.bam
samtools_view_filter.RS_IgG.cd6927b194a618a35ce01622c6d58a37.mugqic.done
)
samtools_view_filter_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: samtools_view_filter_11_JOB_ID: samtools_view_filter_report
#-------------------------------------------------------------------------------
JOB_NAME=samtools_view_filter_report
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID:$samtools_view_filter_2_JOB_ID:$samtools_view_filter_3_JOB_ID:$samtools_view_filter_4_JOB_ID:$samtools_view_filter_5_JOB_ID:$samtools_view_filter_6_JOB_ID:$samtools_view_filter_7_JOB_ID:$samtools_view_filter_8_JOB_ID:$samtools_view_filter_9_JOB_ID:$samtools_view_filter_10_JOB_ID
JOB_DONE=job_output/samtools_view_filter/samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
mkdir -p report && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  --variable min_mapq="20" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.samtools_view_filter.md \
  > report/ChipSeq.samtools_view_filter.md
samtools_view_filter_report.b5cac64c2a4ab579900138ddfbed49a6.mugqic.done
)
samtools_view_filter_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$samtools_view_filter_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_merge_sam_files
#-------------------------------------------------------------------------------
STEP=picard_merge_sam_files
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_1_JOB_ID: symlink_readset_sample_bam.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.H3K27Ac-DHT
JOB_DEPENDENCIES=$samtools_view_filter_1_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.H3K27Ac-DHT.4d59349163a21dc256306f17f5ae0b7d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.H3K27Ac-DHT.4d59349163a21dc256306f17f5ae0b7d.mugqic.done'
mkdir -p alignment/H3K27Ac-DHT && \
ln -s -f RS_H3K27Ac-DHT/RS_H3K27Ac-DHT.sorted.filtered.bam alignment/H3K27Ac-DHT/H3K27Ac-DHT.merged.bam
symlink_readset_sample_bam.H3K27Ac-DHT.4d59349163a21dc256306f17f5ae0b7d.mugqic.done
)
picard_merge_sam_files_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_2_JOB_ID: symlink_readset_sample_bam.H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.H3K27AcWithDHT
JOB_DEPENDENCIES=$samtools_view_filter_2_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.H3K27AcWithDHT.9bb7deb5660aa8bc4f7d9fc6e0931121.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.H3K27AcWithDHT.9bb7deb5660aa8bc4f7d9fc6e0931121.mugqic.done'
mkdir -p alignment/H3K27AcWithDHT && \
ln -s -f RS_H3K27AcWithDHT/RS_H3K27AcWithDHT.sorted.filtered.bam alignment/H3K27AcWithDHT/H3K27AcWithDHT.merged.bam
symlink_readset_sample_bam.H3K27AcWithDHT.9bb7deb5660aa8bc4f7d9fc6e0931121.mugqic.done
)
picard_merge_sam_files_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_3_JOB_ID: symlink_readset_sample_bam.InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.InputDNA1
JOB_DEPENDENCIES=$samtools_view_filter_3_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.InputDNA1.89d6ce4623b3af70e2a052b038b47170.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.InputDNA1.89d6ce4623b3af70e2a052b038b47170.mugqic.done'
mkdir -p alignment/InputDNA1 && \
ln -s -f RS_InputDNA1/RS_InputDNA1.sorted.filtered.bam alignment/InputDNA1/InputDNA1.merged.bam
symlink_readset_sample_bam.InputDNA1.89d6ce4623b3af70e2a052b038b47170.mugqic.done
)
picard_merge_sam_files_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_4_JOB_ID: symlink_readset_sample_bam.InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.InputDNA2
JOB_DEPENDENCIES=$samtools_view_filter_4_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.InputDNA2.684b987a8aced9ad7f19665f7111c0f7.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.InputDNA2.684b987a8aced9ad7f19665f7111c0f7.mugqic.done'
mkdir -p alignment/InputDNA2 && \
ln -s -f RS_InputDNA2/RS_InputDNA2.sorted.filtered.bam alignment/InputDNA2/InputDNA2.merged.bam
symlink_readset_sample_bam.InputDNA2.684b987a8aced9ad7f19665f7111c0f7.mugqic.done
)
picard_merge_sam_files_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_5_JOB_ID: symlink_readset_sample_bam.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$samtools_view_filter_5_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.H3K27ac-dht-siCTRL.1d0aeef573217b6aa3fc41f9bdceb34e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.H3K27ac-dht-siCTRL.1d0aeef573217b6aa3fc41f9bdceb34e.mugqic.done'
mkdir -p alignment/H3K27ac-dht-siCTRL && \
ln -s -f RS_LNCaP-H3K27ac-dht-siCTRL/RS_LNCaP-H3K27ac-dht-siCTRL.sorted.filtered.bam alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.merged.bam
symlink_readset_sample_bam.H3K27ac-dht-siCTRL.1d0aeef573217b6aa3fc41f9bdceb34e.mugqic.done
)
picard_merge_sam_files_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_6_JOB_ID: symlink_readset_sample_bam.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$samtools_view_filter_6_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.H3K27ac-dht-siFoxA1.fb8d705ca3a15a0f4be6443f46a7f7ca.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.H3K27ac-dht-siFoxA1.fb8d705ca3a15a0f4be6443f46a7f7ca.mugqic.done'
mkdir -p alignment/H3K27ac-dht-siFoxA1 && \
ln -s -f RS_LNCaP-H3K27ac-dht-siFoxA1/RS_LNCaP-H3K27ac-dht-siFoxA1.sorted.filtered.bam alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.merged.bam
symlink_readset_sample_bam.H3K27ac-dht-siFoxA1.fb8d705ca3a15a0f4be6443f46a7f7ca.mugqic.done
)
picard_merge_sam_files_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_7_JOB_ID: symlink_readset_sample_bam.Input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Input-dht1
JOB_DEPENDENCIES=$samtools_view_filter_7_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Input-dht1.0b792f9c3e386ee4f23b22c0b4a69bff.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Input-dht1.0b792f9c3e386ee4f23b22c0b4a69bff.mugqic.done'
mkdir -p alignment/Input-dht1 && \
ln -s -f RS_LNCaP-input-dht1/RS_LNCaP-input-dht1.sorted.filtered.bam alignment/Input-dht1/Input-dht1.merged.bam
symlink_readset_sample_bam.Input-dht1.0b792f9c3e386ee4f23b22c0b4a69bff.mugqic.done
)
picard_merge_sam_files_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_8_JOB_ID: symlink_readset_sample_bam.Input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.Input-dht2
JOB_DEPENDENCIES=$samtools_view_filter_8_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.Input-dht2.17360cce7bdfe031a7a4d9c05068fb34.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.Input-dht2.17360cce7bdfe031a7a4d9c05068fb34.mugqic.done'
mkdir -p alignment/Input-dht2 && \
ln -s -f RS_LNCaP-input-dht2/RS_LNCaP-input-dht2.sorted.filtered.bam alignment/Input-dht2/Input-dht2.merged.bam
symlink_readset_sample_bam.Input-dht2.17360cce7bdfe031a7a4d9c05068fb34.mugqic.done
)
picard_merge_sam_files_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_9_JOB_ID: symlink_readset_sample_bam.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.H3K27ac
JOB_DEPENDENCIES=$samtools_view_filter_9_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.H3K27ac.90b6fd562f991e3ba63b4327ccec8490.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.H3K27ac.90b6fd562f991e3ba63b4327ccec8490.mugqic.done'
mkdir -p alignment/H3K27ac && \
ln -s -f RS_H3K27ac/RS_H3K27ac.sorted.filtered.bam alignment/H3K27ac/H3K27ac.merged.bam
symlink_readset_sample_bam.H3K27ac.90b6fd562f991e3ba63b4327ccec8490.mugqic.done
)
picard_merge_sam_files_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_merge_sam_files_10_JOB_ID: symlink_readset_sample_bam.IgG
#-------------------------------------------------------------------------------
JOB_NAME=symlink_readset_sample_bam.IgG
JOB_DEPENDENCIES=$samtools_view_filter_10_JOB_ID
JOB_DONE=job_output/picard_merge_sam_files/symlink_readset_sample_bam.IgG.35eef80942511240cc17dcdee7379a61.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'symlink_readset_sample_bam.IgG.35eef80942511240cc17dcdee7379a61.mugqic.done'
mkdir -p alignment/IgG && \
ln -s -f RS_IgG/RS_IgG.sorted.filtered.bam alignment/IgG/IgG.merged.bam
symlink_readset_sample_bam.IgG.35eef80942511240cc17dcdee7379a61.mugqic.done
)
picard_merge_sam_files_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_merge_sam_files_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: picard_mark_duplicates
#-------------------------------------------------------------------------------
STEP=picard_mark_duplicates
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_1_JOB_ID: picard_mark_duplicates.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H3K27Ac-DHT
JOB_DEPENDENCIES=$picard_merge_sam_files_1_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H3K27Ac-DHT.b1bfd3a640367a23d0aabc7b19ca14b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H3K27Ac-DHT.b1bfd3a640367a23d0aabc7b19ca14b3.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/H3K27Ac-DHT/H3K27Ac-DHT.merged.bam \
  OUTPUT=alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.bam \
  METRICS_FILE=alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.H3K27Ac-DHT.b1bfd3a640367a23d0aabc7b19ca14b3.mugqic.done
)
picard_mark_duplicates_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_2_JOB_ID: picard_mark_duplicates.H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H3K27AcWithDHT
JOB_DEPENDENCIES=$picard_merge_sam_files_2_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H3K27AcWithDHT.1280d4a2d96e809c5379b9ac13a3124b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H3K27AcWithDHT.1280d4a2d96e809c5379b9ac13a3124b.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/H3K27AcWithDHT/H3K27AcWithDHT.merged.bam \
  OUTPUT=alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.bam \
  METRICS_FILE=alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.H3K27AcWithDHT.1280d4a2d96e809c5379b9ac13a3124b.mugqic.done
)
picard_mark_duplicates_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_3_JOB_ID: picard_mark_duplicates.InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.InputDNA1
JOB_DEPENDENCIES=$picard_merge_sam_files_3_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.InputDNA1.250a43bf0364d305d5948584c7a3cb12.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.InputDNA1.250a43bf0364d305d5948584c7a3cb12.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/InputDNA1/InputDNA1.merged.bam \
  OUTPUT=alignment/InputDNA1/InputDNA1.sorted.dup.bam \
  METRICS_FILE=alignment/InputDNA1/InputDNA1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.InputDNA1.250a43bf0364d305d5948584c7a3cb12.mugqic.done
)
picard_mark_duplicates_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_4_JOB_ID: picard_mark_duplicates.InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.InputDNA2
JOB_DEPENDENCIES=$picard_merge_sam_files_4_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.InputDNA2.7ba8c4289b9183999d0bf3164c4febf4.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.InputDNA2.7ba8c4289b9183999d0bf3164c4febf4.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/InputDNA2/InputDNA2.merged.bam \
  OUTPUT=alignment/InputDNA2/InputDNA2.sorted.dup.bam \
  METRICS_FILE=alignment/InputDNA2/InputDNA2.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.InputDNA2.7ba8c4289b9183999d0bf3164c4febf4.mugqic.done
)
picard_mark_duplicates_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_5_JOB_ID: picard_mark_duplicates.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$picard_merge_sam_files_5_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H3K27ac-dht-siCTRL.b1151333a9f80edd2e4ec97751b48aad.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H3K27ac-dht-siCTRL.b1151333a9f80edd2e4ec97751b48aad.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.merged.bam \
  OUTPUT=alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.bam \
  METRICS_FILE=alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.H3K27ac-dht-siCTRL.b1151333a9f80edd2e4ec97751b48aad.mugqic.done
)
picard_mark_duplicates_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_6_JOB_ID: picard_mark_duplicates.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$picard_merge_sam_files_6_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H3K27ac-dht-siFoxA1.fed86da04b0da16352a7829d812578f1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H3K27ac-dht-siFoxA1.fed86da04b0da16352a7829d812578f1.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.merged.bam \
  OUTPUT=alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.bam \
  METRICS_FILE=alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.H3K27ac-dht-siFoxA1.fed86da04b0da16352a7829d812578f1.mugqic.done
)
picard_mark_duplicates_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_7_JOB_ID: picard_mark_duplicates.Input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Input-dht1
JOB_DEPENDENCIES=$picard_merge_sam_files_7_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Input-dht1.429ef63a7ef0ceba1a3195fdf01f5a52.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Input-dht1.429ef63a7ef0ceba1a3195fdf01f5a52.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/Input-dht1/Input-dht1.merged.bam \
  OUTPUT=alignment/Input-dht1/Input-dht1.sorted.dup.bam \
  METRICS_FILE=alignment/Input-dht1/Input-dht1.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Input-dht1.429ef63a7ef0ceba1a3195fdf01f5a52.mugqic.done
)
picard_mark_duplicates_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_8_JOB_ID: picard_mark_duplicates.Input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.Input-dht2
JOB_DEPENDENCIES=$picard_merge_sam_files_8_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.Input-dht2.adba0194ab3b048f423725a5c016d03d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.Input-dht2.adba0194ab3b048f423725a5c016d03d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/Input-dht2/Input-dht2.merged.bam \
  OUTPUT=alignment/Input-dht2/Input-dht2.sorted.dup.bam \
  METRICS_FILE=alignment/Input-dht2/Input-dht2.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.Input-dht2.adba0194ab3b048f423725a5c016d03d.mugqic.done
)
picard_mark_duplicates_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_9_JOB_ID: picard_mark_duplicates.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.H3K27ac
JOB_DEPENDENCIES=$picard_merge_sam_files_9_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.H3K27ac.3a4a45de05920399181027863b4e614d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.H3K27ac.3a4a45de05920399181027863b4e614d.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/H3K27ac/H3K27ac.merged.bam \
  OUTPUT=alignment/H3K27ac/H3K27ac.sorted.dup.bam \
  METRICS_FILE=alignment/H3K27ac/H3K27ac.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.H3K27ac.3a4a45de05920399181027863b4e614d.mugqic.done
)
picard_mark_duplicates_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_10_JOB_ID: picard_mark_duplicates.IgG
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates.IgG
JOB_DEPENDENCIES=$picard_merge_sam_files_10_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates.IgG.826cab47352d157ede617c99b5fd253c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates.IgG.826cab47352d157ede617c99b5fd253c.mugqic.done'
module load mugqic/java/openjdk-jdk1.8.0_72 mugqic/picard/1.123 && \
java -Djava.io.tmpdir=$LSCRATCH -XX:ParallelGCThreads=4 -Xmx20G -jar $PICARD_HOME/MarkDuplicates.jar \
  REMOVE_DUPLICATES=false VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=$LSCRATCH \
  INPUT=alignment/IgG/IgG.merged.bam \
  OUTPUT=alignment/IgG/IgG.sorted.dup.bam \
  METRICS_FILE=alignment/IgG/IgG.sorted.dup.metrics \
  MAX_RECORDS_IN_RAM=1000000
picard_mark_duplicates.IgG.826cab47352d157ede617c99b5fd253c.mugqic.done
)
picard_mark_duplicates_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: picard_mark_duplicates_11_JOB_ID: picard_mark_duplicates_report
#-------------------------------------------------------------------------------
JOB_NAME=picard_mark_duplicates_report
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/picard_mark_duplicates/picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done'
mkdir -p report && \
cp \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.picard_mark_duplicates.md \
  report/ChipSeq.picard_mark_duplicates.md
picard_mark_duplicates_report.c80ab57aaab7999422d809ae0583b4b5.mugqic.done
)
picard_mark_duplicates_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$picard_mark_duplicates_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: metrics
#-------------------------------------------------------------------------------
STEP=metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: metrics_1_JOB_ID: metrics.flagstat
#-------------------------------------------------------------------------------
JOB_NAME=metrics.flagstat
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/metrics/metrics.flagstat.56e82ecf1dbbae2d56ad0af1399c48e1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics.flagstat.56e82ecf1dbbae2d56ad0af1399c48e1.mugqic.done'
module load mugqic/samtools/1.3 && \
samtools flagstat \
  alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.bam \
  > alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.bam \
  > alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/InputDNA1/InputDNA1.sorted.dup.bam \
  > alignment/InputDNA1/InputDNA1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/InputDNA2/InputDNA2.sorted.dup.bam \
  > alignment/InputDNA2/InputDNA2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.bam \
  > alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.bam \
  > alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/Input-dht1/Input-dht1.sorted.dup.bam \
  > alignment/Input-dht1/Input-dht1.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/Input-dht2/Input-dht2.sorted.dup.bam \
  > alignment/Input-dht2/Input-dht2.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/H3K27ac/H3K27ac.sorted.dup.bam \
  > alignment/H3K27ac/H3K27ac.sorted.dup.bam.flagstat && \
samtools flagstat \
  alignment/IgG/IgG.sorted.dup.bam \
  > alignment/IgG/IgG.sorted.dup.bam.flagstat
metrics.flagstat.56e82ecf1dbbae2d56ad0af1399c48e1.mugqic.done
)
metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: metrics_2_JOB_ID: metrics_report
#-------------------------------------------------------------------------------
JOB_NAME=metrics_report
JOB_DEPENDENCIES=$metrics_1_JOB_ID
JOB_DONE=job_output/metrics/metrics_report.8ac8d9c3de1c835ec7a8657933ef4459.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'metrics_report.8ac8d9c3de1c835ec7a8657933ef4459.mugqic.done'
module load mugqic/pandoc/1.15.2 && \
for sample in H3K27Ac-DHT H3K27AcWithDHT InputDNA1 InputDNA2 H3K27ac-dht-siCTRL H3K27ac-dht-siFoxA1 Input-dht1 Input-dht2 H3K27ac IgG
do
  flagstat_file=alignment/$sample/$sample.sorted.dup.bam.flagstat
  echo -e "$sample	`grep -P '^\d+ \+ \d+ mapped' $flagstat_file | grep -Po '^\d+'`	`grep -P '^\d+ \+ \d+ duplicate' $flagstat_file | grep -Po '^\d+'`"
done | \
awk -F"	" '{OFS="	"; print $0, $3 / $2 * 100}' | sed '1iSample	Aligned Filtered Reads	Duplicate Reads	Duplicate %' \
  > metrics/SampleMetrics.stats && \
mkdir -p report && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F "	" 'FNR==NR{trim_line[$1]=$0; surviving[$1]=$3; next}{OFS="	"; if ($1=="Sample") {print trim_line[$1], $2, "Aligned Filtered %", $3, $4} else {print trim_line[$1], $2, $2 / surviving[$1] * 100, $3, $4}}' metrics/trimSampleTable.tsv metrics/SampleMetrics.stats \
  > report/trimMemSampleTable.tsv
else
  cp metrics/SampleMetrics.stats report/trimMemSampleTable.tsv
fi && \
trim_mem_sample_table=`if [[ -f metrics/trimSampleTable.tsv ]] ; then LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8)}}' report/trimMemSampleTable.tsv ; else LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4)}}' report/trimMemSampleTable.tsv ; fi` && \
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  --variable trim_mem_sample_table="$trim_mem_sample_table" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.metrics.md \
  > report/ChipSeq.metrics.md

metrics_report.8ac8d9c3de1c835ec7a8657933ef4459.mugqic.done
)
metrics_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$metrics_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_tag_directory
#-------------------------------------------------------------------------------
STEP=homer_make_tag_directory
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_1_JOB_ID: homer_make_tag_directory.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.H3K27Ac-DHT
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.H3K27Ac-DHT.dcdfee03bb1a0578943e9f7d3a402db0.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.H3K27Ac-DHT.dcdfee03bb1a0578943e9f7d3a402db0.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/H3K27Ac-DHT \
  alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.H3K27Ac-DHT.dcdfee03bb1a0578943e9f7d3a402db0.mugqic.done
)
homer_make_tag_directory_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_2_JOB_ID: homer_make_tag_directory.H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.H3K27AcWithDHT
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.H3K27AcWithDHT.e541f3369b6c9543e166b5b1d7db781c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.H3K27AcWithDHT.e541f3369b6c9543e166b5b1d7db781c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/H3K27AcWithDHT \
  alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.H3K27AcWithDHT.e541f3369b6c9543e166b5b1d7db781c.mugqic.done
)
homer_make_tag_directory_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_3_JOB_ID: homer_make_tag_directory.InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.InputDNA1
JOB_DEPENDENCIES=$picard_mark_duplicates_3_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.InputDNA1.33d2e3e2a21452e83a8db5b134d7d320.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.InputDNA1.33d2e3e2a21452e83a8db5b134d7d320.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/InputDNA1 \
  alignment/InputDNA1/InputDNA1.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.InputDNA1.33d2e3e2a21452e83a8db5b134d7d320.mugqic.done
)
homer_make_tag_directory_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_4_JOB_ID: homer_make_tag_directory.InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.InputDNA2
JOB_DEPENDENCIES=$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.InputDNA2.1d598f5acfd4b70e99990d195aa90789.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.InputDNA2.1d598f5acfd4b70e99990d195aa90789.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/InputDNA2 \
  alignment/InputDNA2/InputDNA2.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.InputDNA2.1d598f5acfd4b70e99990d195aa90789.mugqic.done
)
homer_make_tag_directory_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_5_JOB_ID: homer_make_tag_directory.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.H3K27ac-dht-siCTRL.089b07d54a98268a88bcf40b4b307451.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.H3K27ac-dht-siCTRL.089b07d54a98268a88bcf40b4b307451.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/H3K27ac-dht-siCTRL \
  alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.H3K27ac-dht-siCTRL.089b07d54a98268a88bcf40b4b307451.mugqic.done
)
homer_make_tag_directory_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_6_JOB_ID: homer_make_tag_directory.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.H3K27ac-dht-siFoxA1.444e189019bc813bb8cac0f0a23ebe0c.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.H3K27ac-dht-siFoxA1.444e189019bc813bb8cac0f0a23ebe0c.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/H3K27ac-dht-siFoxA1 \
  alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.H3K27ac-dht-siFoxA1.444e189019bc813bb8cac0f0a23ebe0c.mugqic.done
)
homer_make_tag_directory_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_7_JOB_ID: homer_make_tag_directory.Input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.Input-dht1
JOB_DEPENDENCIES=$picard_mark_duplicates_7_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.Input-dht1.4fa1d51c6fcb654b25240a38b90fd750.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.Input-dht1.4fa1d51c6fcb654b25240a38b90fd750.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/Input-dht1 \
  alignment/Input-dht1/Input-dht1.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.Input-dht1.4fa1d51c6fcb654b25240a38b90fd750.mugqic.done
)
homer_make_tag_directory_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_8_JOB_ID: homer_make_tag_directory.Input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.Input-dht2
JOB_DEPENDENCIES=$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.Input-dht2.c5280723531a541ca0b9f0dd98e1e46e.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.Input-dht2.c5280723531a541ca0b9f0dd98e1e46e.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/Input-dht2 \
  alignment/Input-dht2/Input-dht2.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.Input-dht2.c5280723531a541ca0b9f0dd98e1e46e.mugqic.done
)
homer_make_tag_directory_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_9_JOB_ID: homer_make_tag_directory.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.H3K27ac
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.H3K27ac.c0958cb951d07d93fc8fc961c474ab19.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.H3K27ac.c0958cb951d07d93fc8fc961c474ab19.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/H3K27ac \
  alignment/H3K27ac/H3K27ac.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.H3K27ac.c0958cb951d07d93fc8fc961c474ab19.mugqic.done
)
homer_make_tag_directory_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_tag_directory_10_JOB_ID: homer_make_tag_directory.IgG
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_tag_directory.IgG
JOB_DEPENDENCIES=$picard_mark_duplicates_10_JOB_ID
JOB_DONE=job_output/homer_make_tag_directory/homer_make_tag_directory.IgG.691a03cbd713560e45a41dc72cb18210.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_tag_directory.IgG.691a03cbd713560e45a41dc72cb18210.mugqic.done'
module load mugqic/samtools/1.3 mugqic/homer/4.7 && \
makeTagDirectory \
  tags/IgG \
  alignment/IgG/IgG.sorted.dup.bam \
  -checkGC -genome hg19
homer_make_tag_directory.IgG.691a03cbd713560e45a41dc72cb18210.mugqic.done
)
homer_make_tag_directory_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_tag_directory_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: qc_metrics
#-------------------------------------------------------------------------------
STEP=qc_metrics
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: qc_metrics_1_JOB_ID: qc_plots_R
#-------------------------------------------------------------------------------
JOB_NAME=qc_plots_R
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID:$homer_make_tag_directory_2_JOB_ID:$homer_make_tag_directory_3_JOB_ID:$homer_make_tag_directory_4_JOB_ID:$homer_make_tag_directory_5_JOB_ID:$homer_make_tag_directory_6_JOB_ID:$homer_make_tag_directory_7_JOB_ID:$homer_make_tag_directory_8_JOB_ID:$homer_make_tag_directory_9_JOB_ID:$homer_make_tag_directory_10_JOB_ID
JOB_DONE=job_output/qc_metrics/qc_plots_R.09e703677892720cffd59e6eada7164d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'qc_plots_R.09e703677892720cffd59e6eada7164d.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 mugqic/R_Bioconductor/3.2.3_3.2 && \
mkdir -p graphs && \
Rscript $R_TOOLS/chipSeqGenerateQCMetrics.R \
  ../../design_prostate.txt \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/output/Prostate && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.qc_metrics.md report/ChipSeq.qc_metrics.md && \
for sample in H3K27Ac-DHT H3K27AcWithDHT InputDNA1 InputDNA2 H3K27ac-dht-siCTRL H3K27ac-dht-siFoxA1 Input-dht1 Input-dht2 H3K27ac IgG
do
  cp --parents graphs/${sample}_QC_Metrics.ps report/
  convert -rotate 90 graphs/${sample}_QC_Metrics.ps report/graphs/${sample}_QC_Metrics.png
  echo -e "----

![QC Metrics for Sample $sample ([download high-res image](graphs/${sample}_QC_Metrics.ps))](graphs/${sample}_QC_Metrics.png)
" \
  >> report/ChipSeq.qc_metrics.md
done
qc_plots_R.09e703677892720cffd59e6eada7164d.mugqic.done
)
qc_metrics_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$qc_metrics_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_make_ucsc_file
#-------------------------------------------------------------------------------
STEP=homer_make_ucsc_file
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_1_JOB_ID: homer_make_ucsc_file.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.H3K27Ac-DHT
JOB_DEPENDENCIES=$homer_make_tag_directory_1_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.H3K27Ac-DHT.4f1c54ec5cb1c3cf5d1fc3dc0d1a7ba9.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.H3K27Ac-DHT.4f1c54ec5cb1c3cf5d1fc3dc0d1a7ba9.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/H3K27Ac-DHT && \
makeUCSCfile \
  tags/H3K27Ac-DHT | \
gzip -1 -c > tracks/H3K27Ac-DHT/H3K27Ac-DHT.ucsc.bedGraph.gz
homer_make_ucsc_file.H3K27Ac-DHT.4f1c54ec5cb1c3cf5d1fc3dc0d1a7ba9.mugqic.done
)
homer_make_ucsc_file_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_2_JOB_ID: homer_make_ucsc_file.H3K27AcWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.H3K27AcWithDHT
JOB_DEPENDENCIES=$homer_make_tag_directory_2_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.H3K27AcWithDHT.e7f36a8237bf06cd60a0af785f2d1cbf.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.H3K27AcWithDHT.e7f36a8237bf06cd60a0af785f2d1cbf.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/H3K27AcWithDHT && \
makeUCSCfile \
  tags/H3K27AcWithDHT | \
gzip -1 -c > tracks/H3K27AcWithDHT/H3K27AcWithDHT.ucsc.bedGraph.gz
homer_make_ucsc_file.H3K27AcWithDHT.e7f36a8237bf06cd60a0af785f2d1cbf.mugqic.done
)
homer_make_ucsc_file_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_3_JOB_ID: homer_make_ucsc_file.InputDNA1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.InputDNA1
JOB_DEPENDENCIES=$homer_make_tag_directory_3_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.InputDNA1.0034d53a345b8c8b4bfb38ee3d9b1b72.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.InputDNA1.0034d53a345b8c8b4bfb38ee3d9b1b72.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/InputDNA1 && \
makeUCSCfile \
  tags/InputDNA1 | \
gzip -1 -c > tracks/InputDNA1/InputDNA1.ucsc.bedGraph.gz
homer_make_ucsc_file.InputDNA1.0034d53a345b8c8b4bfb38ee3d9b1b72.mugqic.done
)
homer_make_ucsc_file_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_4_JOB_ID: homer_make_ucsc_file.InputDNA2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.InputDNA2
JOB_DEPENDENCIES=$homer_make_tag_directory_4_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.InputDNA2.901a17c6e4a5dfea6f732ab8cfefae34.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.InputDNA2.901a17c6e4a5dfea6f732ab8cfefae34.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/InputDNA2 && \
makeUCSCfile \
  tags/InputDNA2 | \
gzip -1 -c > tracks/InputDNA2/InputDNA2.ucsc.bedGraph.gz
homer_make_ucsc_file.InputDNA2.901a17c6e4a5dfea6f732ab8cfefae34.mugqic.done
)
homer_make_ucsc_file_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_5_JOB_ID: homer_make_ucsc_file.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$homer_make_tag_directory_5_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.H3K27ac-dht-siCTRL.7145ee824d88a349d5d34a24df20da09.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.H3K27ac-dht-siCTRL.7145ee824d88a349d5d34a24df20da09.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/H3K27ac-dht-siCTRL && \
makeUCSCfile \
  tags/H3K27ac-dht-siCTRL | \
gzip -1 -c > tracks/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.ucsc.bedGraph.gz
homer_make_ucsc_file.H3K27ac-dht-siCTRL.7145ee824d88a349d5d34a24df20da09.mugqic.done
)
homer_make_ucsc_file_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_6_JOB_ID: homer_make_ucsc_file.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$homer_make_tag_directory_6_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.H3K27ac-dht-siFoxA1.b90a22c2d6204eddd1ae613109eb5d93.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.H3K27ac-dht-siFoxA1.b90a22c2d6204eddd1ae613109eb5d93.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/H3K27ac-dht-siFoxA1 && \
makeUCSCfile \
  tags/H3K27ac-dht-siFoxA1 | \
gzip -1 -c > tracks/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.ucsc.bedGraph.gz
homer_make_ucsc_file.H3K27ac-dht-siFoxA1.b90a22c2d6204eddd1ae613109eb5d93.mugqic.done
)
homer_make_ucsc_file_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_7_JOB_ID: homer_make_ucsc_file.Input-dht1
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.Input-dht1
JOB_DEPENDENCIES=$homer_make_tag_directory_7_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.Input-dht1.58f80453e6862f86fb5473df780e595d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.Input-dht1.58f80453e6862f86fb5473df780e595d.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/Input-dht1 && \
makeUCSCfile \
  tags/Input-dht1 | \
gzip -1 -c > tracks/Input-dht1/Input-dht1.ucsc.bedGraph.gz
homer_make_ucsc_file.Input-dht1.58f80453e6862f86fb5473df780e595d.mugqic.done
)
homer_make_ucsc_file_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_8_JOB_ID: homer_make_ucsc_file.Input-dht2
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.Input-dht2
JOB_DEPENDENCIES=$homer_make_tag_directory_8_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.Input-dht2.b81c1fe66ae4fbeaf3b393266b1124fd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.Input-dht2.b81c1fe66ae4fbeaf3b393266b1124fd.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/Input-dht2 && \
makeUCSCfile \
  tags/Input-dht2 | \
gzip -1 -c > tracks/Input-dht2/Input-dht2.ucsc.bedGraph.gz
homer_make_ucsc_file.Input-dht2.b81c1fe66ae4fbeaf3b393266b1124fd.mugqic.done
)
homer_make_ucsc_file_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_9_JOB_ID: homer_make_ucsc_file.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.H3K27ac
JOB_DEPENDENCIES=$homer_make_tag_directory_9_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.H3K27ac.f9631b20831cc5309abbafde0243b873.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.H3K27ac.f9631b20831cc5309abbafde0243b873.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/H3K27ac && \
makeUCSCfile \
  tags/H3K27ac | \
gzip -1 -c > tracks/H3K27ac/H3K27ac.ucsc.bedGraph.gz
homer_make_ucsc_file.H3K27ac.f9631b20831cc5309abbafde0243b873.mugqic.done
)
homer_make_ucsc_file_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_10_JOB_ID: homer_make_ucsc_file.IgG
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file.IgG
JOB_DEPENDENCIES=$homer_make_tag_directory_10_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file.IgG.fdfa0a2eb709bb0fa9f8fffd0eae2219.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file.IgG.fdfa0a2eb709bb0fa9f8fffd0eae2219.mugqic.done'
module load mugqic/homer/4.7 && \
mkdir -p tracks/IgG && \
makeUCSCfile \
  tags/IgG | \
gzip -1 -c > tracks/IgG/IgG.ucsc.bedGraph.gz
homer_make_ucsc_file.IgG.fdfa0a2eb709bb0fa9f8fffd0eae2219.mugqic.done
)
homer_make_ucsc_file_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_make_ucsc_file_11_JOB_ID: homer_make_ucsc_file_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_make_ucsc_file_report
JOB_DEPENDENCIES=$homer_make_ucsc_file_10_JOB_ID
JOB_DONE=job_output/homer_make_ucsc_file/homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done'
mkdir -p report && \
zip -r report/tracks.zip tracks/*/*.ucsc.bedGraph.gz && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.homer_make_ucsc_file.md report/
homer_make_ucsc_file_report.c9cac04b3bc71f34cbcde042cf5b419d.mugqic.done
)
homer_make_ucsc_file_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_make_ucsc_file_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: macs2_callpeak
#-------------------------------------------------------------------------------
STEP=macs2_callpeak
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_1_JOB_ID: macs2_callpeak.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.H3K27Ac-DHT
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.H3K27Ac-DHT.19ef9eb482428659e2aaddd4955324b8.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.H3K27Ac-DHT.19ef9eb482428659e2aaddd4955324b8.mugqic.done'
module load mugqic/python/2.7.11 mugqic/MACS2/2.1.0.20151222 && \
mkdir -p peak_call/H3K27Ac-DHT && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2509729011.2 \
  --treatment \
  alignment/H3K27Ac-DHT/H3K27Ac-DHT.sorted.dup.bam \
  --control \
  alignment/InputDNA1/InputDNA1.sorted.dup.bam \
  alignment/InputDNA2/InputDNA2.sorted.dup.bam \
  --name peak_call/H3K27Ac-DHT/H3K27Ac-DHT \
  >& peak_call/H3K27Ac-DHT/H3K27Ac-DHT.diag.macs.out
macs2_callpeak.H3K27Ac-DHT.19ef9eb482428659e2aaddd4955324b8.mugqic.done
)
macs2_callpeak_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_2_JOB_ID: macs2_callpeak.H3K27cWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.H3K27cWithDHT
JOB_DEPENDENCIES=$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.H3K27cWithDHT.dcd005015000eb1277a4f8ba96aa0f39.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.H3K27cWithDHT.dcd005015000eb1277a4f8ba96aa0f39.mugqic.done'
module load mugqic/python/2.7.11 mugqic/MACS2/2.1.0.20151222 && \
mkdir -p peak_call/H3K27cWithDHT && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2509729011.2 \
  --treatment \
  alignment/H3K27AcWithDHT/H3K27AcWithDHT.sorted.dup.bam \
  --control \
  alignment/InputDNA1/InputDNA1.sorted.dup.bam \
  alignment/InputDNA2/InputDNA2.sorted.dup.bam \
  --name peak_call/H3K27cWithDHT/H3K27cWithDHT \
  >& peak_call/H3K27cWithDHT/H3K27cWithDHT.diag.macs.out
macs2_callpeak.H3K27cWithDHT.dcd005015000eb1277a4f8ba96aa0f39.mugqic.done
)
macs2_callpeak_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_3_JOB_ID: macs2_callpeak.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.H3K27ac-dht-siCTRL.9a8691716b0830bd820bfb3b4f68fa0a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.H3K27ac-dht-siCTRL.9a8691716b0830bd820bfb3b4f68fa0a.mugqic.done'
module load mugqic/python/2.7.11 mugqic/MACS2/2.1.0.20151222 && \
mkdir -p peak_call/H3K27ac-dht-siCTRL && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2509729011.2 \
  --treatment \
  alignment/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.sorted.dup.bam \
  --control \
  alignment/Input-dht1/Input-dht1.sorted.dup.bam \
  alignment/Input-dht2/Input-dht2.sorted.dup.bam \
  --name peak_call/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL \
  >& peak_call/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.diag.macs.out
macs2_callpeak.H3K27ac-dht-siCTRL.9a8691716b0830bd820bfb3b4f68fa0a.mugqic.done
)
macs2_callpeak_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_4_JOB_ID: macs2_callpeak.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.H3K27ac-dht-siFoxA1.db9acfe9681d557e2cad5d19c243c190.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.H3K27ac-dht-siFoxA1.db9acfe9681d557e2cad5d19c243c190.mugqic.done'
module load mugqic/python/2.7.11 mugqic/MACS2/2.1.0.20151222 && \
mkdir -p peak_call/H3K27ac-dht-siFoxA1 && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2509729011.2 \
  --treatment \
  alignment/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.sorted.dup.bam \
  --control \
  alignment/Input-dht1/Input-dht1.sorted.dup.bam \
  alignment/Input-dht2/Input-dht2.sorted.dup.bam \
  --name peak_call/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1 \
  >& peak_call/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.diag.macs.out
macs2_callpeak.H3K27ac-dht-siFoxA1.db9acfe9681d557e2cad5d19c243c190.mugqic.done
)
macs2_callpeak_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_5_JOB_ID: macs2_callpeak.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak.H3K27ac
JOB_DEPENDENCIES=$picard_mark_duplicates_9_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak.H3K27ac.9aa5246cc84da3d8e5399e839c135553.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak.H3K27ac.9aa5246cc84da3d8e5399e839c135553.mugqic.done'
module load mugqic/python/2.7.11 mugqic/MACS2/2.1.0.20151222 && \
mkdir -p peak_call/H3K27ac && \
macs2 callpeak --format BAM --broad --nomodel \
  --gsize 2509729011.2 \
  --treatment \
  alignment/H3K27ac/H3K27ac.sorted.dup.bam \
  --nolambda \
  --name peak_call/H3K27ac/H3K27ac \
  >& peak_call/H3K27ac/H3K27ac.diag.macs.out
macs2_callpeak.H3K27ac.9aa5246cc84da3d8e5399e839c135553.mugqic.done
)
macs2_callpeak_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: macs2_callpeak_6_JOB_ID: macs2_callpeak_report
#-------------------------------------------------------------------------------
JOB_NAME=macs2_callpeak_report
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID:$macs2_callpeak_2_JOB_ID:$macs2_callpeak_3_JOB_ID:$macs2_callpeak_4_JOB_ID:$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/macs2_callpeak/macs2_callpeak_report.d74e7f9859cc1ba0c058bca23134cb7a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'macs2_callpeak_report.d74e7f9859cc1ba0c058bca23134cb7a.mugqic.done'
mkdir -p report && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.macs2_callpeak.md report/ && \
for contrast in H3K27Ac-DHT H3K27cWithDHT H3K27ac-dht-siCTRL H3K27ac-dht-siFoxA1 H3K27ac
do
  cp -a --parents peak_call/$contrast/ report/ && \
  echo -e "* [Peak Calls File for Design $contrast](peak_call/$contrast/${contrast}_peaks.xls)" \
  >> report/ChipSeq.macs2_callpeak.md
done
macs2_callpeak_report.d74e7f9859cc1ba0c058bca23134cb7a.mugqic.done
)
macs2_callpeak_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$macs2_callpeak_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_annotate_peaks
#-------------------------------------------------------------------------------
STEP=homer_annotate_peaks
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_1_JOB_ID: homer_annotate_peaks.H3K27Ac-DHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks.H3K27Ac-DHT
JOB_DEPENDENCIES=$macs2_callpeak_1_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks.H3K27Ac-DHT.432f53e7ad83bebe1488d04d309640be.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks.H3K27Ac-DHT.432f53e7ad83bebe1488d04d309640be.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.7 mugqic/mugqic_tools/2.1.5 && \
mkdir -p annotation/H3K27Ac-DHT/H3K27Ac-DHT && \
annotatePeaks.pl \
  peak_call/H3K27Ac-DHT/H3K27Ac-DHT_peaks.broadPeak \
  hg19 \
  -gsize hg19 \
  -cons -CpG \
  -go annotation/H3K27Ac-DHT/H3K27Ac-DHT \
  -genomeOntology annotation/H3K27Ac-DHT/H3K27Ac-DHT \
  > annotation/H3K27Ac-DHT/H3K27Ac-DHT.annotated.csv && \
perl -MReadMetrics -e 'ReadMetrics::parseHomerAnnotations(
  "annotation/H3K27Ac-DHT/H3K27Ac-DHT.annotated.csv",
  "annotation/H3K27Ac-DHT/H3K27Ac-DHT",
  -2000,
  -10000,
  -10000,
  -100000,
  100000
)'
homer_annotate_peaks.H3K27Ac-DHT.432f53e7ad83bebe1488d04d309640be.mugqic.done
)
homer_annotate_peaks_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_2_JOB_ID: homer_annotate_peaks.H3K27cWithDHT
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks.H3K27cWithDHT
JOB_DEPENDENCIES=$macs2_callpeak_2_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks.H3K27cWithDHT.92698028e74e0e8b9e4350ccdc94c963.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks.H3K27cWithDHT.92698028e74e0e8b9e4350ccdc94c963.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.7 mugqic/mugqic_tools/2.1.5 && \
mkdir -p annotation/H3K27cWithDHT/H3K27cWithDHT && \
annotatePeaks.pl \
  peak_call/H3K27cWithDHT/H3K27cWithDHT_peaks.broadPeak \
  hg19 \
  -gsize hg19 \
  -cons -CpG \
  -go annotation/H3K27cWithDHT/H3K27cWithDHT \
  -genomeOntology annotation/H3K27cWithDHT/H3K27cWithDHT \
  > annotation/H3K27cWithDHT/H3K27cWithDHT.annotated.csv && \
perl -MReadMetrics -e 'ReadMetrics::parseHomerAnnotations(
  "annotation/H3K27cWithDHT/H3K27cWithDHT.annotated.csv",
  "annotation/H3K27cWithDHT/H3K27cWithDHT",
  -2000,
  -10000,
  -10000,
  -100000,
  100000
)'
homer_annotate_peaks.H3K27cWithDHT.92698028e74e0e8b9e4350ccdc94c963.mugqic.done
)
homer_annotate_peaks_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_3_JOB_ID: homer_annotate_peaks.H3K27ac-dht-siCTRL
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks.H3K27ac-dht-siCTRL
JOB_DEPENDENCIES=$macs2_callpeak_3_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks.H3K27ac-dht-siCTRL.faf428cb8088d1ed800cb0754bb325d1.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks.H3K27ac-dht-siCTRL.faf428cb8088d1ed800cb0754bb325d1.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.7 mugqic/mugqic_tools/2.1.5 && \
mkdir -p annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL && \
annotatePeaks.pl \
  peak_call/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL_peaks.broadPeak \
  hg19 \
  -gsize hg19 \
  -cons -CpG \
  -go annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL \
  -genomeOntology annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL \
  > annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.annotated.csv && \
perl -MReadMetrics -e 'ReadMetrics::parseHomerAnnotations(
  "annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL.annotated.csv",
  "annotation/H3K27ac-dht-siCTRL/H3K27ac-dht-siCTRL",
  -2000,
  -10000,
  -10000,
  -100000,
  100000
)'
homer_annotate_peaks.H3K27ac-dht-siCTRL.faf428cb8088d1ed800cb0754bb325d1.mugqic.done
)
homer_annotate_peaks_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_4_JOB_ID: homer_annotate_peaks.H3K27ac-dht-siFoxA1
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks.H3K27ac-dht-siFoxA1
JOB_DEPENDENCIES=$macs2_callpeak_4_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks.H3K27ac-dht-siFoxA1.de39197c73c5baf02e55315bf46ded64.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks.H3K27ac-dht-siFoxA1.de39197c73c5baf02e55315bf46ded64.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.7 mugqic/mugqic_tools/2.1.5 && \
mkdir -p annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1 && \
annotatePeaks.pl \
  peak_call/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1_peaks.broadPeak \
  hg19 \
  -gsize hg19 \
  -cons -CpG \
  -go annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1 \
  -genomeOntology annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1 \
  > annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.annotated.csv && \
perl -MReadMetrics -e 'ReadMetrics::parseHomerAnnotations(
  "annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1.annotated.csv",
  "annotation/H3K27ac-dht-siFoxA1/H3K27ac-dht-siFoxA1",
  -2000,
  -10000,
  -10000,
  -100000,
  100000
)'
homer_annotate_peaks.H3K27ac-dht-siFoxA1.de39197c73c5baf02e55315bf46ded64.mugqic.done
)
homer_annotate_peaks_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_5_JOB_ID: homer_annotate_peaks.H3K27ac
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks.H3K27ac
JOB_DEPENDENCIES=$macs2_callpeak_5_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks.H3K27ac.a80da90655f5495e19746f1ed4397428.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks.H3K27ac.a80da90655f5495e19746f1ed4397428.mugqic.done'
module load mugqic/perl/5.22.1 mugqic/homer/4.7 mugqic/mugqic_tools/2.1.5 && \
mkdir -p annotation/H3K27ac/H3K27ac && \
annotatePeaks.pl \
  peak_call/H3K27ac/H3K27ac_peaks.broadPeak \
  hg19 \
  -gsize hg19 \
  -cons -CpG \
  -go annotation/H3K27ac/H3K27ac \
  -genomeOntology annotation/H3K27ac/H3K27ac \
  > annotation/H3K27ac/H3K27ac.annotated.csv && \
perl -MReadMetrics -e 'ReadMetrics::parseHomerAnnotations(
  "annotation/H3K27ac/H3K27ac.annotated.csv",
  "annotation/H3K27ac/H3K27ac",
  -2000,
  -10000,
  -10000,
  -100000,
  100000
)'
homer_annotate_peaks.H3K27ac.a80da90655f5495e19746f1ed4397428.mugqic.done
)
homer_annotate_peaks_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: homer_annotate_peaks_6_JOB_ID: homer_annotate_peaks_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_annotate_peaks_report
JOB_DEPENDENCIES=$homer_annotate_peaks_1_JOB_ID:$homer_annotate_peaks_2_JOB_ID:$homer_annotate_peaks_3_JOB_ID:$homer_annotate_peaks_4_JOB_ID:$homer_annotate_peaks_5_JOB_ID
JOB_DONE=job_output/homer_annotate_peaks/homer_annotate_peaks_report.178aecd68d7a89d060b97861769df5b3.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_annotate_peaks_report.178aecd68d7a89d060b97861769df5b3.mugqic.done'
mkdir -p report/annotation/ && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.homer_annotate_peaks.md report/ && \
for contrast in H3K27Ac-DHT H3K27cWithDHT H3K27ac-dht-siCTRL H3K27ac-dht-siFoxA1 H3K27ac
do
  rsync -avP annotation/$contrast report/annotation/ && \
  echo -e "* [Gene Annotations for Design $contrast](annotation/$contrast/${contrast}.annotated.csv)
* [HOMER Gene Ontology Annotations for Design $contrast](annotation/$contrast/$contrast/geneOntology.html)
* [HOMER Genome Ontology Annotations for Design $contrast](annotation/$contrast/$contrast/GenomeOntology.html)" \
  >> report/ChipSeq.homer_annotate_peaks.md
done
homer_annotate_peaks_report.178aecd68d7a89d060b97861769df5b3.mugqic.done
)
homer_annotate_peaks_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$homer_annotate_peaks_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: homer_find_motifs_genome
#-------------------------------------------------------------------------------
STEP=homer_find_motifs_genome
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: homer_find_motifs_genome_1_JOB_ID: homer_find_motifs_genome_report
#-------------------------------------------------------------------------------
JOB_NAME=homer_find_motifs_genome_report
JOB_DEPENDENCIES=
JOB_DONE=job_output/homer_find_motifs_genome/homer_find_motifs_genome_report.959158a103da242fb5beccc945aea64d.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'homer_find_motifs_genome_report.959158a103da242fb5beccc945aea64d.mugqic.done'
mkdir -p report/annotation/ && \
cp /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.homer_find_motifs_genome.md report/ && \
for contrast in 
do
  rsync -avP annotation/$contrast report/annotation/ && \
  echo -e "* [HOMER _De Novo_ Motif Results for Design $contrast](annotation/$contrast/$contrast/homerResults.html)
* [HOMER Known Motif Results for Design $contrast](annotation/$contrast/$contrast/knownResults.html)" \
  >> report/ChipSeq.homer_find_motifs_genome.md
done
homer_find_motifs_genome_report.959158a103da242fb5beccc945aea64d.mugqic.done
)
homer_find_motifs_genome_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 | grep "[0-9]")
echo "$homer_find_motifs_genome_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: annotation_graphs
#-------------------------------------------------------------------------------
STEP=annotation_graphs
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: annotation_graphs_1_JOB_ID: annotation_graphs
#-------------------------------------------------------------------------------
JOB_NAME=annotation_graphs
JOB_DEPENDENCIES=$homer_annotate_peaks_1_JOB_ID:$homer_annotate_peaks_2_JOB_ID:$homer_annotate_peaks_3_JOB_ID:$homer_annotate_peaks_4_JOB_ID:$homer_annotate_peaks_5_JOB_ID
JOB_DONE=job_output/annotation_graphs/annotation_graphs.8e6458448172a69265d6e0bc55036d52.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'annotation_graphs.8e6458448172a69265d6e0bc55036d52.mugqic.done'
module load mugqic/mugqic_tools/2.1.5 mugqic/R_Bioconductor/3.2.3_3.2 mugqic/pandoc/1.15.2 && \
mkdir -p graphs && \
Rscript $R_TOOLS/chipSeqgenerateAnnotationGraphs.R \
  ../../design_prostate.txt \
  /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/output/Prostate && \
mkdir -p report/annotation/ && \
if [[ -f annotation/peak_stats.csv ]]
then
  cp annotation/peak_stats.csv report/annotation/
peak_stats_table=`LC_NUMERIC=en_CA awk -F "," '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, $2,  sprintf("%\47d", $3), $4, sprintf("%\47.1f", $5), sprintf("%\47.1f", $6), sprintf("%\47.1f", $7), sprintf("%\47.1f", $8)}}' annotation/peak_stats.csv`
else
  peak_stats_table=""
fi
pandoc --to=markdown \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.annotation_graphs.md \
  --variable peak_stats_table="$peak_stats_table" \
  --variable proximal_distance="2" \
  --variable distal_distance="10" \
  --variable distance5d_lower="10" \
  --variable distance5d_upper="100" \
  --variable gene_desert_size="100" \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.2.0/bfx/report/ChipSeq.annotation_graphs.md \
  > report/ChipSeq.annotation_graphs.md && \
for contrast in 
do
  cp --parents graphs/${contrast}_Misc_Graphs.ps report/
  convert -rotate 90 graphs/${contrast}_Misc_Graphs.ps report/graphs/${contrast}_Misc_Graphs.png
  echo -e "----

![Annotation Statistics for Design $contrast ([download high-res image](graphs/${contrast}_Misc_Graphs.ps))](graphs/${contrast}_Misc_Graphs.png)
" \
  >> report/ChipSeq.annotation_graphs.md
done
annotation_graphs.8e6458448172a69265d6e0bc55036d52.mugqic.done
)
annotation_graphs_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -W group_list=$RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=120:00:0 -q qwork -l nodes=1:ppn=1 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$annotation_graphs_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# Call home with pipeline statistics
#-------------------------------------------------------------------------------
wget "http://mugqic.hpc.mcgill.ca/cgi-bin/pipeline.cgi?hostname=ip01&ip=10.0.120.101&pipeline=ChipSeq&steps=picard_sam_to_fastq,trimmomatic,merge_trimmomatic_stats,bwa_mem_picard_sort_sam,samtools_view_filter,picard_merge_sam_files,picard_mark_duplicates,metrics,homer_make_tag_directory,qc_metrics,homer_make_ucsc_file,macs2_callpeak,homer_annotate_peaks,homer_find_motifs_genome,annotation_graphs&samples=10" --quiet --output-document=/dev/null

