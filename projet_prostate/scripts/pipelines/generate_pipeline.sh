export RAP_ID=fhq-091-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/output
$MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.py -s '1-22' -l debug \
    -r $PROJECT_BASE/input/readset.txt \
    -d $PROJECT_BASE/input/design.txt \
    -o $SCRATCH/projet_prostate/output \
    --config $PROJECT_BASE//rnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
        $MUGQIC_PIPELINES_HOME/resources/genomes/config/Homo_sapiens.GRCh38.ini \
        ./this_run.ini