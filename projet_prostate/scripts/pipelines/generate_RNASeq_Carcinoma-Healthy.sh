export RAP_ID=vyt-470-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/Carcinoma-Healthy
/home/efournier/mcgill_pipelines/mugqic_pipelines/pipelines/rnaseq/rnaseq.py -s '1-14,22' -l debug \
    -r $PROJECT_BASE/input/config/Carcinoma-Healthy/readset.txt \
    -d $PROJECT_BASE/input/config/Carcinoma-Healthy/design.txt \
    -o $SCRATCH/projet_prostate/Carcinoma-Healthy \
    --config $PROJECT_BASE/input/config/Carcinoma-Healthy/rnaseq.base.ini \
        $PROJECT_BASE/input/config/Carcinoma-Healthy/rnaseq.guillimin.ini \
        $PROJECT_BASE/input/config/Carcinoma-Healthy/Homo_sapiens.GRCh38.ERCC.ini \
        $PROJECT_BASE/input/config/Carcinoma-Healthy/this_run.ini