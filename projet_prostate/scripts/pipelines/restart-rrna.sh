#!/bin/bash
# Exit immediately on error
set -eu -o pipefail

#-------------------------------------------------------------------------------
# RnaSeq PBSScheduler Job Submission Bash script
# Version: 2.1.1
# Created on: 2016-02-15T13:18:50
# Steps:
#   picard_sam_to_fastq: 0 job... skipping
#   trimmomatic: 0 job... skipping
#   merge_trimmomatic_stats: 0 job... skipping
#   star: 24 jobs
#   picard_merge_sam_files: 0 job... skipping
#   picard_sort_sam: 16 jobs
#   picard_mark_duplicates: 16 jobs
#   picard_rna_metrics: 16 jobs
#   estimate_ribosomal_rna: 16 jobs
#   rnaseqc: 2 jobs
#   wiggle: 64 jobs
#   raw_counts: 16 jobs
#   raw_counts_metrics: 4 jobs
#   cufflinks: 16 jobs
#   cuffmerge: 1 job
#   cuffquant: 16 jobs
#   cuffdiff: 3 jobs
#   cuffnorm: 1 job
#   fpkm_correlation_matrix: 2 jobs
#   gq_seq_utils_exploratory_analysis_rnaseq: 3 jobs
#   differential_expression: 1 job
#   differential_expression_goseq: 4 jobs
#   TOTAL: 221 jobs
#-------------------------------------------------------------------------------

OUTPUT_DIR=/gs/scratch/efournier/projet_prostate/output
JOB_OUTPUT_DIR=$OUTPUT_DIR/job_output
TIMESTAMP=`date +%FT%H.%M.%S`
JOB_LIST=$JOB_OUTPUT_DIR/RnaSeq_job_list_$TIMESTAMP
mkdir -p $OUTPUT_DIR
cd $OUTPUT_DIR




#-------------------------------------------------------------------------------
# STEP: estimate_ribosomal_rna
#-------------------------------------------------------------------------------
STEP=estimate_ribosomal_rna
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_1_JOB_ID: bwa_mem_rRNA.NoCancer-26
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.NoCancer-26
JOB_DEPENDENCIES=$star_8_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.NoCancer-26.3c467f75570bd16361dcf635c2413942.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.NoCancer-26.3c467f75570bd16361dcf635c2413942.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_6-26/NoCancer-26 metrics/Gleason_6-26/NoCancer-26 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_6-26/NoCancer-26/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:NoCancer-26	SM:Gleason_6-26	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_6-26/NoCancer-26/NoCancer-26rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_6-26/NoCancer-26/NoCancer-26rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_6-26/NoCancer-26/NoCancer-26rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.NoCancer-26.3c467f75570bd16361dcf635c2413942.mugqic.done
)
estimate_ribosomal_rna_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_2_JOB_ID: bwa_mem_rRNA.Gleason_6-28
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_6-28
JOB_DEPENDENCIES=$star_9_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_6-28.8c7c33ffd51a854428925a541f805e5a.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_6-28.8c7c33ffd51a854428925a541f805e5a.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_6-28/Gleason_6-28 metrics/Gleason_6-28/Gleason_6-28 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_6-28/Gleason_6-28/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_6-28	SM:Gleason_6-28	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_6-28/Gleason_6-28/Gleason_6-28rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_6-28/Gleason_6-28/Gleason_6-28rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_6-28/Gleason_6-28/Gleason_6-28rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_6-28.8c7c33ffd51a854428925a541f805e5a.mugqic.done
)
estimate_ribosomal_rna_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_3_JOB_ID: bwa_mem_rRNA.Gleason_6-35
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_6-35
JOB_DEPENDENCIES=$star_10_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_6-35.5d6155edd9f28cf9ac2d72aa864bfe16.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_6-35.5d6155edd9f28cf9ac2d72aa864bfe16.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_6-35/Gleason_6-35 metrics/Gleason_6-35/Gleason_6-35 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_6-35/Gleason_6-35/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_6-35	SM:Gleason_6-35	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_6-35/Gleason_6-35/Gleason_6-35rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_6-35/Gleason_6-35/Gleason_6-35rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_6-35/Gleason_6-35/Gleason_6-35rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_6-35.5d6155edd9f28cf9ac2d72aa864bfe16.mugqic.done
)
estimate_ribosomal_rna_3_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_3_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_4_JOB_ID: bwa_mem_rRNA.Gleason_6-40
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_6-40
JOB_DEPENDENCIES=$star_11_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_6-40.360b39aa45ade4094af69fbde8946e62.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_6-40.360b39aa45ade4094af69fbde8946e62.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_6-40/Gleason_6-40 metrics/Gleason_6-40/Gleason_6-40 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_6-40/Gleason_6-40/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_6-40	SM:Gleason_6-40	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_6-40/Gleason_6-40/Gleason_6-40rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_6-40/Gleason_6-40/Gleason_6-40rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_6-40/Gleason_6-40/Gleason_6-40rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_6-40.360b39aa45ade4094af69fbde8946e62.mugqic.done
)
estimate_ribosomal_rna_4_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_4_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_5_JOB_ID: bwa_mem_rRNA.Gleason_8-14
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_8-14
JOB_DEPENDENCIES=$star_12_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_8-14.98c1d93b28c1160a6904f7fe969fa330.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_8-14.98c1d93b28c1160a6904f7fe969fa330.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_8-14/Gleason_8-14 metrics/Gleason_8-14/Gleason_8-14 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_8-14/Gleason_8-14/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_8-14	SM:Gleason_8-14	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_8-14/Gleason_8-14/Gleason_8-14rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_8-14/Gleason_8-14/Gleason_8-14rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_8-14/Gleason_8-14/Gleason_8-14rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_8-14.98c1d93b28c1160a6904f7fe969fa330.mugqic.done
)
estimate_ribosomal_rna_5_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_5_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_6_JOB_ID: bwa_mem_rRNA.Gleason_8-8
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_8-8
JOB_DEPENDENCIES=$star_13_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_8-8.05cdc9b6db03d96c38a2a9b1057ce7ce.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_8-8.05cdc9b6db03d96c38a2a9b1057ce7ce.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_8-8/Gleason_8-8 metrics/Gleason_8-8/Gleason_8-8 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_8-8/Gleason_8-8/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_8-8	SM:Gleason_8-8	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_8-8/Gleason_8-8/Gleason_8-8rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_8-8/Gleason_8-8/Gleason_8-8rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_8-8/Gleason_8-8/Gleason_8-8rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_8-8.05cdc9b6db03d96c38a2a9b1057ce7ce.mugqic.done
)
estimate_ribosomal_rna_6_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_6_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_7_JOB_ID: bwa_mem_rRNA.Gleason_8-84
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_8-84
JOB_DEPENDENCIES=$star_14_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_8-84.b398903e1011ab9e20771bfa9e89f749.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_8-84.b398903e1011ab9e20771bfa9e89f749.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_8-84/Gleason_8-84 metrics/Gleason_8-84/Gleason_8-84 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_8-84/Gleason_8-84/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_8-84	SM:Gleason_8-84	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_8-84/Gleason_8-84/Gleason_8-84rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_8-84/Gleason_8-84/Gleason_8-84rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_8-84/Gleason_8-84/Gleason_8-84rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_8-84.b398903e1011ab9e20771bfa9e89f749.mugqic.done
)
estimate_ribosomal_rna_7_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_7_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_8_JOB_ID: bwa_mem_rRNA.Gleason_8-87
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Gleason_8-87
JOB_DEPENDENCIES=$star_15_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Gleason_8-87.306b9a442947ff15038a758fefca8067.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Gleason_8-87.306b9a442947ff15038a758fefca8067.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Gleason_8-87/Gleason_8-87 metrics/Gleason_8-87/Gleason_8-87 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Gleason_8-87/Gleason_8-87/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Gleason_8-87	SM:Gleason_8-87	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Gleason_8-87/Gleason_8-87/Gleason_8-87rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Gleason_8-87/Gleason_8-87/Gleason_8-87rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Gleason_8-87/Gleason_8-87/Gleason_8-87rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Gleason_8-87.306b9a442947ff15038a758fefca8067.mugqic.done
)
estimate_ribosomal_rna_8_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_8_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_9_JOB_ID: bwa_mem_rRNA.NoCancer-19
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.NoCancer-19
JOB_DEPENDENCIES=$star_16_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.NoCancer-19.1382b03c6c6d6af03aecbff20b3e5923.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.NoCancer-19.1382b03c6c6d6af03aecbff20b3e5923.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/NoCancer-19/NoCancer-19 metrics/NoCancer-19/NoCancer-19 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/NoCancer-19/NoCancer-19/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:NoCancer-19	SM:NoCancer-19	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/NoCancer-19/NoCancer-19/NoCancer-19rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/NoCancer-19/NoCancer-19/NoCancer-19rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/NoCancer-19/NoCancer-19/NoCancer-19rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.NoCancer-19.1382b03c6c6d6af03aecbff20b3e5923.mugqic.done
)
estimate_ribosomal_rna_9_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_9_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_10_JOB_ID: bwa_mem_rRNA.NoCancer-53
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.NoCancer-53
JOB_DEPENDENCIES=$star_17_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.NoCancer-53.abee4d459d9b1b43967ed64a2712e719.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.NoCancer-53.abee4d459d9b1b43967ed64a2712e719.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/NoCancer-53/NoCancer-53 metrics/NoCancer-53/NoCancer-53 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/NoCancer-53/NoCancer-53/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:NoCancer-53	SM:NoCancer-53	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/NoCancer-53/NoCancer-53/NoCancer-53rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/NoCancer-53/NoCancer-53/NoCancer-53rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/NoCancer-53/NoCancer-53/NoCancer-53rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.NoCancer-53.abee4d459d9b1b43967ed64a2712e719.mugqic.done
)
estimate_ribosomal_rna_10_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_10_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_11_JOB_ID: bwa_mem_rRNA.NoCancer-64
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.NoCancer-64
JOB_DEPENDENCIES=$star_18_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.NoCancer-64.7b05bc68bef0cb5ae0204411a8a3b2ba.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.NoCancer-64.7b05bc68bef0cb5ae0204411a8a3b2ba.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/NoCancer-64/NoCancer-64 metrics/NoCancer-64/NoCancer-64 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/NoCancer-64/NoCancer-64/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:NoCancer-64	SM:NoCancer-64	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/NoCancer-64/NoCancer-64/NoCancer-64rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/NoCancer-64/NoCancer-64/NoCancer-64rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/NoCancer-64/NoCancer-64/NoCancer-64rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.NoCancer-64.7b05bc68bef0cb5ae0204411a8a3b2ba.mugqic.done
)
estimate_ribosomal_rna_11_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_11_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_12_JOB_ID: bwa_mem_rRNA.NoCancer-79
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.NoCancer-79
JOB_DEPENDENCIES=$star_19_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.NoCancer-79.1c9c52d6ecdead7e573d57391780e2dd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.NoCancer-79.1c9c52d6ecdead7e573d57391780e2dd.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/NoCancer-79/NoCancer-79 metrics/NoCancer-79/NoCancer-79 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/NoCancer-79/NoCancer-79/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:NoCancer-79	SM:NoCancer-79	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/NoCancer-79/NoCancer-79/NoCancer-79rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/NoCancer-79/NoCancer-79/NoCancer-79rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/NoCancer-79/NoCancer-79/NoCancer-79rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.NoCancer-79.1c9c52d6ecdead7e573d57391780e2dd.mugqic.done
)
estimate_ribosomal_rna_12_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_12_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_13_JOB_ID: bwa_mem_rRNA.Normal-44
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Normal-44
JOB_DEPENDENCIES=$star_20_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Normal-44.b91c5b34b4768e34b78c27c06632b65f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Normal-44.b91c5b34b4768e34b78c27c06632b65f.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Normal-44/Normal-44 metrics/Normal-44/Normal-44 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Normal-44/Normal-44/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Normal-44	SM:Normal-44	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Normal-44/Normal-44/Normal-44rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Normal-44/Normal-44/Normal-44rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Normal-44/Normal-44/Normal-44rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Normal-44.b91c5b34b4768e34b78c27c06632b65f.mugqic.done
)
estimate_ribosomal_rna_13_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_13_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_14_JOB_ID: bwa_mem_rRNA.Normal-62
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Normal-62
JOB_DEPENDENCIES=$star_21_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Normal-62.bc71a7ea6207997b061be4dd6c75bdbd.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Normal-62.bc71a7ea6207997b061be4dd6c75bdbd.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Normal-62/Normal-62 metrics/Normal-62/Normal-62 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Normal-62/Normal-62/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Normal-62	SM:Normal-62	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Normal-62/Normal-62/Normal-62rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Normal-62/Normal-62/Normal-62rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Normal-62/Normal-62/Normal-62rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Normal-62.bc71a7ea6207997b061be4dd6c75bdbd.mugqic.done
)
estimate_ribosomal_rna_14_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_14_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_15_JOB_ID: bwa_mem_rRNA.Normal-65
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Normal-65
JOB_DEPENDENCIES=$star_22_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Normal-65.bfea79dda78fa23379b265b8afa20b00.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Normal-65.bfea79dda78fa23379b265b8afa20b00.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Normal-65/Normal-65 metrics/Normal-65/Normal-65 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Normal-65/Normal-65/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Normal-65	SM:Normal-65	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Normal-65/Normal-65/Normal-65rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Normal-65/Normal-65/Normal-65rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Normal-65/Normal-65/Normal-65rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Normal-65.bfea79dda78fa23379b265b8afa20b00.mugqic.done
)
estimate_ribosomal_rna_15_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_15_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: estimate_ribosomal_rna_16_JOB_ID: bwa_mem_rRNA.Normal-69
#-------------------------------------------------------------------------------
JOB_NAME=bwa_mem_rRNA.Normal-69
JOB_DEPENDENCIES=$star_23_JOB_ID
JOB_DONE=job_output/estimate_ribosomal_rna/bwa_mem_rRNA.Normal-69.8051624eae524ae708fd39064995f020.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'bwa_mem_rRNA.Normal-69.8051624eae524ae708fd39064995f020.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bvatools/1.5 mugqic/bwa/0.7.10 mugqic/picard/1.123 mugqic/mugqic_tools/2.1.5 mugqic/python/2.7.8 && \
mkdir -p alignment/Normal-69/Normal-69 metrics/Normal-69/Normal-69 && \
java -XX:ParallelGCThreads=1 -Xmx10G -jar $BVATOOLS_JAR \
  bam2fq --mapped ONLY \
  --bam alignment/Normal-69/Normal-69/Aligned.sortedByCoord.out.bam    | \
bwa mem  \
  -M -t 10 \
  -R '@RG	ID:Normal-69	SM:Normal-69	CN:McGill University and Genome Quebec Innovation Centre	PL:Illumina' \
  $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/rrna_bwa_index/Homo_sapiens.GRCh38.Ensembl77.rrna.fa \
  /dev/stdin | \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx7G -jar $PICARD_HOME/SortSam.jar \
  VALIDATION_STRINGENCY=SILENT CREATE_INDEX=true \
  TMP_DIR=/gs/scratch/$USER \
  INPUT=/dev/stdin \
  OUTPUT=metrics/Normal-69/Normal-69/Normal-69rRNA.bam \
  SORT_ORDER=coordinate \
  MAX_RECORDS_IN_RAM=1750000 && \
python $PYTHON_TOOLS/rrnaBAMcounter.py \
  -i metrics/Normal-69/Normal-69/Normal-69rRNA.bam \
  -g $MUGQIC_INSTALL_HOME/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.gtf \
  -o metrics/Normal-69/Normal-69/Normal-69rRNA.stats.tsv \
  -t transcript
bwa_mem_rRNA.Normal-69.8051624eae524ae708fd39064995f020.mugqic.done
)
estimate_ribosomal_rna_16_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$estimate_ribosomal_rna_16_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# STEP: rnaseqc
#-------------------------------------------------------------------------------
STEP=rnaseqc
mkdir -p $JOB_OUTPUT_DIR/$STEP


#-------------------------------------------------------------------------------
# JOB: rnaseqc_1_JOB_ID: rnaseqc
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc
JOB_DEPENDENCIES=$picard_mark_duplicates_1_JOB_ID:$picard_mark_duplicates_2_JOB_ID:$picard_mark_duplicates_3_JOB_ID:$picard_mark_duplicates_4_JOB_ID:$picard_mark_duplicates_5_JOB_ID:$picard_mark_duplicates_6_JOB_ID:$picard_mark_duplicates_7_JOB_ID:$picard_mark_duplicates_8_JOB_ID:$picard_mark_duplicates_9_JOB_ID:$picard_mark_duplicates_10_JOB_ID:$picard_mark_duplicates_11_JOB_ID:$picard_mark_duplicates_12_JOB_ID:$picard_mark_duplicates_13_JOB_ID:$picard_mark_duplicates_14_JOB_ID:$picard_mark_duplicates_15_JOB_ID:$picard_mark_duplicates_16_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc.5314416f9f6c237d5912ab176ac8362f.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc.5314416f9f6c237d5912ab176ac8362f.mugqic.done'
module load mugqic/java/openjdk-jdk1.7.0_60 mugqic/bwa/0.7.10 mugqic/rnaseqc/1.1.7 && \
mkdir -p metrics/rnaseqRep && \
echo "Sample	BamFile	Note
Gleason_6-26	alignment/Gleason_6-26/Gleason_6-26.sorted.mdup.bam	RNAseq
Gleason_6-28	alignment/Gleason_6-28/Gleason_6-28.sorted.mdup.bam	RNAseq
Gleason_6-35	alignment/Gleason_6-35/Gleason_6-35.sorted.mdup.bam	RNAseq
Gleason_6-40	alignment/Gleason_6-40/Gleason_6-40.sorted.mdup.bam	RNAseq
Gleason_8-14	alignment/Gleason_8-14/Gleason_8-14.sorted.mdup.bam	RNAseq
Gleason_8-8	alignment/Gleason_8-8/Gleason_8-8.sorted.mdup.bam	RNAseq
Gleason_8-84	alignment/Gleason_8-84/Gleason_8-84.sorted.mdup.bam	RNAseq
Gleason_8-87	alignment/Gleason_8-87/Gleason_8-87.sorted.mdup.bam	RNAseq
NoCancer-19	alignment/NoCancer-19/NoCancer-19.sorted.mdup.bam	RNAseq
NoCancer-53	alignment/NoCancer-53/NoCancer-53.sorted.mdup.bam	RNAseq
NoCancer-64	alignment/NoCancer-64/NoCancer-64.sorted.mdup.bam	RNAseq
NoCancer-79	alignment/NoCancer-79/NoCancer-79.sorted.mdup.bam	RNAseq
Normal-44	alignment/Normal-44/Normal-44.sorted.mdup.bam	RNAseq
Normal-62	alignment/Normal-62/Normal-62.sorted.mdup.bam	RNAseq
Normal-65	alignment/Normal-65/Normal-65.sorted.mdup.bam	RNAseq
Normal-69	alignment/Normal-69/Normal-69.sorted.mdup.bam	RNAseq" \
  > alignment/rnaseqc.samples.txt && \
java -Djava.io.tmpdir=/gs/scratch/$USER -XX:ParallelGCThreads=1 -Xmx27G -jar $RNASEQC_JAR \
  -n 1000 \
  -o metrics/rnaseqRep \
  -r /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/genome/Homo_sapiens.GRCh38.fa \
  -s alignment/rnaseqc.samples.txt \
  -t /cvmfs/soft.mugqic/CentOS6/genomes/species/Homo_sapiens.GRCh38/annotations/Homo_sapiens.GRCh38.Ensembl77.transcript_id.gtf \
  -ttype 2 && \
zip -r metrics/rnaseqRep.zip metrics/rnaseqRep
rnaseqc.5314416f9f6c237d5912ab176ac8362f.mugqic.done
)
rnaseqc_1_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=12 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$rnaseqc_1_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST


#-------------------------------------------------------------------------------
# JOB: rnaseqc_2_JOB_ID: rnaseqc_report
#-------------------------------------------------------------------------------
JOB_NAME=rnaseqc_report
JOB_DEPENDENCIES=$rnaseqc_1_JOB_ID
JOB_DONE=job_output/rnaseqc/rnaseqc_report.1f34e6d475e7a6029ec66e88d3767d1b.mugqic.done
JOB_OUTPUT_RELATIVE_PATH=$STEP/${JOB_NAME}_$TIMESTAMP.o
JOB_OUTPUT=$JOB_OUTPUT_DIR/$JOB_OUTPUT_RELATIVE_PATH
COMMAND=$(cat << 'rnaseqc_report.1f34e6d475e7a6029ec66e88d3767d1b.mugqic.done'
module load mugqic/python/2.7.8 mugqic/pandoc/1.13.1 && \
mkdir -p report && \
cp metrics/rnaseqRep.zip report/reportRNAseqQC.zip && \
python -c 'import csv; csv_in = csv.DictReader(open("metrics/rnaseqRep/metrics.tsv"), delimiter="	")
print "	".join(["Sample", "Aligned Reads", "Alternative Alignments", "%", "rRNA Reads", "Coverage", "Exonic Rate", "Genes"])
print "\n".join(["	".join([
    line["Sample"],
    line["Mapped"],
    line["Alternative Aligments"],
    str(float(line["Alternative Aligments"]) / float(line["Mapped"]) * 100),
    line["rRNA"],
    line["Mean Per Base Cov."],
    line["Exonic Rate"],
    line["Genes Detected"]
]) for line in csv_in])' \
  > report/trimAlignmentTable.tsv.tmp && \
if [[ -f metrics/trimSampleTable.tsv ]]
then
  awk -F"	" 'FNR==NR{raw_reads[$1]=$2; surviving_reads[$1]=$3; surviving_pct[$1]=$4; next}{OFS="	"; if ($2=="Aligned Reads"){surviving_pct[$1]="%"; aligned_pct="%"; rrna_pct="%"} else {aligned_pct=($2 / surviving_reads[$1] * 100); rrna_pct=($5 / surviving_reads[$1] * 100)}; printf $1"	"raw_reads[$1]"	"surviving_reads[$1]"	"surviving_pct[$1]"	"$2"	"aligned_pct"	"$3"	"$4"	"$5"	"rrna_pct; for (i = 6; i<= NF; i++) {printf "	"$i}; print ""}' \
  metrics/trimSampleTable.tsv \
  report/trimAlignmentTable.tsv.tmp \
  > report/trimAlignmentTable.tsv
else
  cp report/trimAlignmentTable.tsv.tmp report/trimAlignmentTable.tsv
fi && \
rm report/trimAlignmentTable.tsv.tmp && \
trim_alignment_table_md=`if [[ -f metrics/trimSampleTable.tsv ]] ; then cut -f1-13 report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.1f", $6), sprintf("%\47d", $7), sprintf("%.1f", $8), sprintf("%\47d", $9), sprintf("%.1f", $10), sprintf("%.2f", $11), sprintf("%.2f", $12), sprintf("%\47d", $13)}}' ; else cat report/trimAlignmentTable.tsv | LC_NUMERIC=en_CA awk -F "	" '{OFS="|"; if (NR == 1) {$1 = $1; print $0; print "-----|-----:|-----:|-----:|-----:|-----:|-----:|-----:"} else {print $1, sprintf("%\47d", $2), sprintf("%\47d", $3), sprintf("%.1f", $4), sprintf("%\47d", $5), sprintf("%.2f", $6), sprintf("%.2f", $7), $8}}' ; fi`
pandoc \
  /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.1/bfx/report/RnaSeq.rnaseqc.md \
  --template /cvmfs/soft.mugqic/CentOS6/software/mugqic_pipelines/mugqic_pipelines-2.1.1/bfx/report/RnaSeq.rnaseqc.md \
  --variable trim_alignment_table="$trim_alignment_table_md" \
  --to markdown \
  > report/RnaSeq.rnaseqc.md
rnaseqc_report.1f34e6d475e7a6029ec66e88d3767d1b.mugqic.done
)
rnaseqc_2_JOB_ID=$(echo "rm -f $JOB_DONE && $COMMAND
MUGQIC_STATE=\$PIPESTATUS
echo MUGQICexitStatus:\$MUGQIC_STATE
if [ \$MUGQIC_STATE -eq 0 ] ; then touch $JOB_DONE ; fi
exit \$MUGQIC_STATE" | \
qsub -m ae -M $JOB_MAIL -W umask=0002 -A $RAP_ID -d $OUTPUT_DIR -j oe -o $JOB_OUTPUT -N $JOB_NAME -l walltime=24:00:0 -q metaq -l nodes=1:ppn=1 -l pmem=2700m -W depend=afterok:$JOB_DEPENDENCIES | grep "[0-9]")
echo "$rnaseqc_2_JOB_ID	$JOB_NAME	$JOB_DEPENDENCIES	$JOB_OUTPUT_RELATIVE_PATH" >> $JOB_LIST

