export RAP_ID=vyt-470-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/basal-luminal
/home/efournier/mcgill_pipelines/mugqic_pipelines/pipelines/rnaseq/rnaseq.py -s '1-22' -l debug \
    -r $PROJECT_BASE/input/config/rna-seq-external/readset.txt \
    -d $PROJECT_BASE/input/config/rna-seq-external/design.txt \
    -o $SCRATCH/projet_prostate/basal-luminal \
    --config $PROJECT_BASE/input/config/rna-seq-external/rnaseq.base.ini \
        $MUGQIC_PIPELINES_HOME/pipelines/rnaseq/rnaseq.guillimin.ini \
        $PROJECT_BASE/input/config/rna-seq-external/Homo_sapiens.GRCh38.ERCC.ini \
        $PROJECT_BASE/input/config/rna-seq-external/this_run.ini