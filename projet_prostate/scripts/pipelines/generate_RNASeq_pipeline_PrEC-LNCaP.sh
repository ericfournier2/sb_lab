export RAP_ID=vyt-470-aa

PROJECT_BASE=/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

mkdir -p $SCRATCH/projet_prostate/PrEC-LNCaP
/home/efournier/mcgill_pipelines/mugqic_pipelines/pipelines/rnaseq/rnaseq.py -s '1-14,22' -l debug \
    -r $PROJECT_BASE/input/config/PrEC-LNCaP/readset.txt \
    -d $PROJECT_BASE/input/config/PrEC-LNCaP/design.txt \
    -o $SCRATCH/projet_prostate/PrEC-LNCaP \
    --config $PROJECT_BASE/input/config/PrEC-LNCaP/rnaseq.base.ini \
        $PROJECT_BASE/input/config/PrEC-LNCaP/rnaseq.guillimin.ini \
        $PROJECT_BASE/input/config/PrEC-LNCaP/Homo_sapiens.GRCh38.ERCC.ini \
        $PROJECT_BASE/input/config/PrEC-LNCaP/this_run.ini