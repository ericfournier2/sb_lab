#!/bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l walltime=8:00:00
#PBS -A fhq-091-aa
#PBS -o run_csaw_prostate.sh.stdout
#PBS -e run_csaw_prostate.sh.stderr
#PBS -V
#PBS -N run_csaw_prostate.sh

cd /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/efournie/ProstateH3K27acexterne/NPEC-ChIP

module load mugqic_dev/R_Bioconductor/3.2.4-revised_3.2
Rscript csaw_for_pipeline.R input/bam.files.prostate.txt Normal hg38 csaw_output
