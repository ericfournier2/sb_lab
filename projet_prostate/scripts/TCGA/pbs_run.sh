#!/bin/bash
#PBS -l nodes=1:ppn=8
#PBS -l walltime=8:00:00
#PBS -A fhq-091-aa
#PBS -o prostate_healthy_tumor.sh.stdout
#PBS -e prostate_healthy_tumor.sh.stderr
#PBS -V
#PBS -N prostate_healthy_tumor

module load mugqic/R_Bioconductor/3.2.3_3.2

cd /gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate

Rscript "scripts/TCGA/Tumor vs normal.R"