ON_SERVER= Sys.info()["sysname"]!="Windows"

# Set working directory.
if(ON_SERVER) {
    common.root="/gs/project/fhq-091-aa/Working_Directory/Eric"
    setwd("/gs/project/fhq-091-aa/Working_Directory/Eric/projet_prostate")
    cache.dir=file.path(common.root, "global_cache")
    shared=file.path(common.root, "shared")
} else if (Sys.info()["nodename"] == "AUDREY-PC") {
    common.root="C:/Users/UL/Desktop/Stage/GitProstate/sb_lab"
    setwd("C:/Users/UL/Desktop/Stage/GitProstate/sb_lab/projet_prostate")
    cache.dir=file.path(common.root, "global_cache")
    shared=file.path(common.root, "shared/R")
} else {
    common.root="C:/Dev/Projects"
    setwd(file.path(common.root, "Cancer du sein"))
    cache.dir=file.path(common.root, "cache")
    shared=file.path(common.root, "shared")
}
GENOME_VERSION="hg19"

library(eric.utils)

if(ON_SERVER) {
    registerCoresPWMEnrich(8)
}

# Load clinical data.
#source("scripts/TCGA/SummarizeClinical.R")
selectedDF = read.table("input/TCGA/clinical.txt")

# Load TCGA expression data.
if(!file.exists("input/TCGA/Expression summary.txt")) {
    dir.create("input/TCGA", showWarnings=FALSE, recursive=TRUE)
    source("scripts/TCGA/SummarizeExpression.R")
}

# Read in expression info which was summarized by SummarizeExpression.R
expr.data = read.table("input/TCGA/Expression summary.txt", sep="\t", check.names = FALSE)

# Read file annotation to identify problem cases.
file.annotations <- read.table("input/TCGA/file_annotations.txt", sep="\t", header=TRUE)

# Build a metadata table.
# Load patient barcode -> file name mapping.
file.manifest = read.table("input/TCGA/file_manifest.txt", sep="\t", header=TRUE)
file.names <- colnames(expr.data)[-c(1,2)]
metadata = file.manifest[match(file.names, as.character(file.manifest$File.Name)),]
metadata$Type = ifelse(substr(metadata$Sample, 14, 14)=="1", "Healthy", "Tumor")

# Get a list of sample barcodes where tumor tissue as present within normal tissue
to.exclude = gsub("[A-Z]$", "",subset(file.annotations, Annotation=="Normal tissue contains tumor")$Item.Barcode)
metadata$Exclude = metadata$Sample %in% to.exclude
metadata = cbind(metadata, selectedDF[match(gsub("-..$", "", metadata$Sample), as.character(selectedDF$patient.barcode)),])

# Log the data and remove the gene headers.
expr.matrix = log2(t(expr.data[, -c(1,2)]) + 1)

# Split data in two
tumor.data = expr.matrix[metadata$Type=="Tumor", ]
healthy.data = expr.matrix[metadata$Type=="Healthy" & !metadata$Exclude, ]
gleason6.data = expr.matrix[metadata$gleason.score=="6" & metadata$Type=="Tumor", ]
gleason7.data = expr.matrix[metadata$gleason.score=="7" & metadata$Type=="Tumor", ]
gleasonHigh.data = expr.matrix[metadata$gleason.category==">=8" & metadata$Type=="Tumor", ]

# Figure out which genes are expressed in either tissue.
expressed.tumor = apply(tumor.data > 0, 2, sum) >= nrow(tumor.data) * 0.9
expressed.healthy = apply(healthy.data > 0, 2, sum) >= nrow(healthy.data) * 0.9
expressed.gleason6 = apply(gleason6.data > 0, 2, sum, na.rm=TRUE) >= nrow(gleason6.data) * 0.9
expressed.gleason7 = apply(gleason7.data > 0, 2, sum, na.rm=TRUE) >= nrow(gleason7.data) * 0.9
expressed.gleasonHigh = apply(gleasonHigh.data > 0, 2, sum, na.rm=TRUE) >= nrow(gleasonHigh.data) * 0.9

# Generate a venn diagram of genes expressed by tissue.
dir.create("output/TCGA", recursive=TRUE)
library(VennDiagram)
venn.diagram(list(Gleason6=which(expressed.gleason6), Gleason7=which(expressed.gleason7),
                  GleasonHigh=which(expressed.gleasonHigh), Healthy=which(expressed.healthy)),
             filename="output/TCGA/Venn of genes expressed by gleason score.tiff")
venn.diagram(list(Tumor=which(expressed.tumor), Healthy=which(expressed.healthy)),
             filename="output/TCGA/Venn of genes expressed by tissue type.tiff")

# Perform t-tests on all genes.
p.values = c()
conf.low = c()
conf.high = c()
for(i in 1:ncol(tumor.data)) {
    test.res = t.test(tumor.data[,i], healthy.data[,i])
    p.values = c(p.values, test.res$p.value)
    conf.low = c(conf.low, test.res$conf.int[1])
    conf.high = c(conf.high, test.res$conf.int[2])
}
adj.p.values = p.adjust(p.values, method="fdr")

# Perform t-tests on all genes, Healthy vs GleasonHigh.
p.values.H_GH = c()
conf.low.H_GH = c()
conf.high.H_GH = c()
for(i in 1:ncol(healthy.data)) {
  test.res.H_GH = t.test(healthy.data[,i], gleasonHigh.data[,i])
  p.values.H_GH = c(p.values.H_GH, test.res.H_GH$p.value)
  conf.low.H_GH = c(conf.low.H_GH, test.res.H_GH$conf.int[1])
  conf.high.H_GH = c(conf.high.H_GH, test.res.H_GH$conf.int[2])
}
adj.p.values.H_GH = p.adjust(p.values.H_GH, method="fdr")

# Perform t-tests on all genes, Healthy vs Gleason7.
p.values.H_G7 = c()
conf.low.H_G7 = c()
conf.high.H_G7 = c()
for(i in 1:ncol(healthy.data)) {
  test.res.H_G7 = t.test(healthy.data[,i], gleason7.data[,i])
  p.values.H_G7 = c(p.values.H_G7, test.res.H_G7$p.value)
  conf.low.H_G7 = c(conf.low.H_G7, test.res.H_G7$conf.int[1])
  conf.high.H_G7 = c(conf.high.H_G7, test.res.H_G7$conf.int[2])
}
adj.p.values.H_G7 = p.adjust(p.values.H_G7, method="fdr")

# Perform t-tests on all genes, Healthy vs Gleason6.
p.values.H_G6 = c()
conf.low.H_G6 = c()
conf.high.H_G6 = c()
for(i in 1:ncol(healthy.data)) {
  test.res.H_G6 = t.test(healthy.data[,i], gleason6.data[,i])
  p.values.H_G6 = c(p.values.H_G6, test.res.H_G6$p.value)
  conf.low.H_G6 = c(conf.low.H_G6, test.res.H_G6$conf.int[1])
  conf.high.H_G6 = c(conf.high.H_G6, test.res.H_G6$conf.int[2])
}
adj.p.values.H_G6 = p.adjust(p.values.H_G6, method="fdr")

# Perform t-tests on all genes, Gleason7 vs Gleason6.
p.values.G7_G6 = c()
conf.low.G7_G6 = c()
conf.high.G7_G6 = c()
for(i in 1:ncol(gleason7.data)) {
  test.res.G7_G6 = t.test(gleason7.data[,i], gleason6.data[,i])
  p.values.G7_G6 = c(p.values.G7_G6, test.res.G7_G6$p.value)
  conf.low.G7_G6 = c(conf.low.G7_G6, test.res.G7_G6$conf.int[1])
  conf.high.G7_G6 = c(conf.high.G7_G6, test.res.G7_G6$conf.int[2])
}
adj.p.values.G7_G6 = p.adjust(p.values.G7_G6, method="fdr")

# Perform t-tests on all genes, GleasonHigh vs Gleason6.
p.values.GH_G6 = c()
conf.low.GH_G6 = c()
conf.high.GH_G6 = c()
for(i in 1:ncol(gleasonHigh.data)) {
  test.res.GH_G6 = t.test(gleasonHigh.data[,i], gleason6.data[,i])
  p.values.GH_G6 = c(p.values.GH_G6, test.res.GH_G6$p.value)
  conf.low.GH_G6 = c(conf.low.GH_G6, test.res.GH_G6$conf.int[1])
  conf.high.GH_G6 = c(conf.high.GH_G6, test.res.GH_G6$conf.int[2])
}
adj.p.values.GH_G6 = p.adjust(p.values.GH_G6, method="fdr")

# Perform t-tests on all genes, GleasonHigh vs Gleason7.
p.values.GH_G7 = c()
conf.low.GH_G7 = c()
conf.high.GH_G7 = c()
for(i in 1:ncol(gleasonHigh.data)) {
  test.res.GH_G7 = t.test(gleasonHigh.data[,i], gleason7.data[,i])
  p.values.GH_G7 = c(p.values.GH_G7, test.res.GH_G7$p.value)
  conf.low.GH_G7 = c(conf.low.GH_G7, test.res.GH_G7$conf.int[1])
  conf.high.GH_G7 = c(conf.high.GH_G7, test.res.GH_G7$conf.int[2])
}
adj.p.values.GH_G7 = p.adjust(p.values.GH_G7, method="fdr")

# Calculate fold-changes.
fc = apply(tumor.data, 2, mean) - apply(healthy.data, 2, mean)
fc.H_GH = apply(healthy.data, 2, mean, na.rm = TRUE) - apply(gleasonHigh.data, 2, mean, na.rm = TRUE)
fc.H_G6 = apply(healthy.data, 2, mean, na.rm = TRUE) - apply(gleason6.data, 2, mean, na.rm = TRUE)
fc.H_G7 = apply(healthy.data, 2, mean, na.rm = TRUE) - apply(gleason7.data, 2, mean, na.rm = TRUE)
fc.GH_G6 = apply(gleasonHigh.data, 2, mean, na.rm = TRUE) - apply(gleason6.data, 2, mean, na.rm = TRUE)
fc.GH_G7 = apply(gleasonHigh.data, 2, mean, na.rm = TRUE) - apply(gleason7.data, 2, mean, na.rm = TRUE)
fc.G7_G6 = apply(gleason7.data, 2, mean, na.rm = TRUE) - apply(gleason6.data, 2, mean, na.rm = TRUE)

# Write out comparative expression data.
healthy.mean = apply(healthy.data, 2, mean, na.rm=TRUE)
tumor.mean = apply(tumor.data, 2, mean, na.rm=TRUE)
all.data = data.frame(Symbol               = expr.data$Symbol,
                      Entrez               = expr.data$Entrez.Gene.ID,
                      Expr.Healthy.Mean    = healthy.mean,
                      Expr.Healthy.SD      = apply(healthy.data, 2, sd, na.rm=TRUE),
                      Expr.Healthy.Rank    = length(healthy.mean) - rank(healthy.mean),
                      Expr.Tumor.Mean      = tumor.mean,
                      Expr.Tumor.SD        = apply(tumor.data, 2, sd, na.rm=TRUE),
                      Expr.Tumor.Rank      = length(tumor.mean) - rank(tumor.mean),
                      Diff.Expr.FC         = fc,
                      Diff.Expr.FC.CI.low  = conf.low,
                      Diff.Expr.FC.CI.high = conf.high,
                      Diff.Expr.Pvalue     = p.values,
                      Diff.Expr.Adj.PValue = adj.p.values)
write.table(all.data, "output/TCGA/Differential Expression Summary.txt", sep="\t", col.names=TRUE, row.names=FALSE)

# Write out comparative expression data, according to clustering
gleasonHigh.mean = apply(gleasonHigh.data, 2, mean, na.rm=TRUE)
gleason6.mean = apply(gleason6.data, 2, mean, na.rm=TRUE)
gleason7.mean = apply(gleason7.data, 2, mean, na.rm=TRUE)
all.data2 = data.frame(Symbol=expr.data$Symbol,
                      Entrez=expr.data$Entrez.Gene.ID,
                      Expr.Healthy.Mean    = healthy.mean,
                      Expr.Healthy.SD      = apply(healthy.data, 2, sd, na.rm=TRUE),
                      Expr.Healthy.Rank    = length(healthy.mean) - rank(healthy.mean),
                      Expr.GleasonHigh.Mean= gleasonHigh.mean,
                      Expr.GleasonHigh.SD  = apply(gleasonHigh.data, 2, sd, na.rm=TRUE),
                      Expr.GleasonHigh.Rank= length(gleasonHigh.mean) - rank(gleasonHigh.mean),
                      Expr.Gleason6.Mean   = gleason6.mean,
                      Expr.Gleason6.SD     = apply(gleason6.data, 2, sd, na.rm=TRUE),
                      Expr.Gleason6.Rank   = length(gleason6.mean) - rank(gleason6.mean),
                      Expr.Gleason7.Mean   = gleason7.mean,
                      Expr.Gleason7.SD     = apply(gleason7.data, 2, sd, na.rm=TRUE),
                      Expr.Gleason7.Rank   = length(gleason7.mean) - rank(gleason6.mean),
                      Diff.Expr.FC.H_GH         = fc.H_GH,
                      Diff.Expr.FC.CI.low.H_GH  = conf.low.H_GH,
                      Diff.Expr.FC.CI.high.H_GH = conf.high.H_GH,
                      Diff.Expr.Pvalue.H_GH     = p.values.H_GH,
                      Diff.Expr.Adj.PValue.H_GH = adj.p.values.H_GH,
                      Diff.Expr.FC.H_G6         = fc.H_G6,
                      Diff.Expr.FC.CI.low.H_G6  = conf.low.H_G6,
                      Diff.Expr.FC.CI.high.H_G6 = conf.high.H_G6,
                      Diff.Expr.Pvalue.H_G6     = p.values.H_G6,
                      Diff.Expr.Adj.PValue.H_G6 = adj.p.values.H_G6,
                      Diff.Expr.FC.H_G7         = fc.H_G7,
                      Diff.Expr.FC.CI.low.H_G7  = conf.low.H_G7,
                      Diff.Expr.FC.CI.high.H_G7 = conf.high.H_G7,
                      Diff.Expr.Pvalue.H_G7     = p.values.H_G7,
                      Diff.Expr.Adj.PValue.H_G7 = adj.p.values.H_G7,
                      Diff.Expr.FC.G7_G6         = fc.G7_G6,
                      Diff.Expr.FC.CI.low.G7_G6  = conf.low.G7_G6,
                      Diff.Expr.FC.CI.high.G7_G6 = conf.high.G7_G6,
                      Diff.Expr.Pvalue.G7_G6     = p.values.G7_G6,
                      Diff.Expr.Adj.PValue.G7_G6 = adj.p.values.G7_G6,
                      Diff.Expr.FC.GH_G6         = fc.GH_G6,
                      Diff.Expr.FC.CI.low.GH_G6  = conf.low.GH_G6,
                      Diff.Expr.FC.CI.high.GH_G6 = conf.high.GH_G6,
                      Diff.Expr.Pvalue.GH_G6     = p.values.GH_G6,
                      Diff.Expr.Adj.PValue.GH_G6 = adj.p.values.GH_G6,
                      Diff.Expr.FC.GH_G7         = fc.GH_G7,
                      Diff.Expr.FC.CI.low.GH_G7  = conf.low.GH_G7,
                      Diff.Expr.FC.CI.high.GH_G7 = conf.high.GH_G7,
                      Diff.Expr.Pvalue.GH_G7     = p.values.GH_G7,
                      Diff.Expr.Adj.PValue.GH_G7 = adj.p.values.GH_G7)
write.table(all.data2, "output/TCGA/Differential Expression Summary (by gleason score).txt", sep="\t", col.names=TRUE, row.names=FALSE)


# Produce a cat-plot, tumor vs normal.
source("scripts/TCGA/catPlotFunctions.R")

expr.for.cat = data.frame(ID=1:nrow(all.data),
                          Healthy=all.data$Expr.Healthy.Mean,
                          Tumor=all.data$Expr.Tumor.Mean)
cat.results = computeCat(expr.for.cat, idCol=1, method="equalRank", decreasing=TRUE)[[1]]$cat
catDF=data.frame(ListSize=1:length(cat.results), Overlap=cat.results)

library(ggplot2)

ggplot(catDF, aes(x=ListSize, y=Overlap*100)) +
    geom_line() +
    ylim(c(0,100)) +
    labs(list(title="Overlap of most expressed tumor and healthy genes", x="List size", y="Overlap")) +
    theme(axis.text.x=element_text(hjust=1),
          panel.background = element_rect(fill='#FFFFFF'),
          axis.line=element_line(colour="black", size=1, linetype="solid"),
          axis.text = element_text(colour="black", size=12),
          panel.grid.major.y = element_line(colour="#F0F0F0", size=1),
          panel.grid.minor.y = element_line(colour="#F4F4F4", size=1))
ggsave(paste0("output/TCGA/CAT plot (all).pdf"), width=7, height=7, units="in", dpi=600)


ggplot(catDF, aes(x=ListSize, y=Overlap*100)) +
    geom_line() +
    ylim(c(0,100)) +
    xlim(c(0,1000)) +
    labs(list(title="Overlap of most expressed tumor and healthy genes", x="List size", y="Overlap")) +
    theme(axis.text.x=element_text(hjust=1),
          panel.background = element_rect(fill='#FFFFFF'),
          axis.line=element_line(colour="black", size=1, linetype="solid"),
          axis.text = element_text(colour="black", size=12),
          panel.grid.major.y = element_line(colour="#F0F0F0", size=1),
          panel.grid.minor.y = element_line(colour="#F4F4F4", size=1))
ggsave(paste0("output/TCGA/CAT plot (1000).pdf"), width=7, height=7, units="in", dpi=600)

# Produce a cat-plot, normal vs gleason scores.
expr.for.cat2 = data.frame(ID=1:nrow(all.data2),
                           Healthy=all.data2$Expr.Healthy.Mean,
                           Gleason6=all.data2$Expr.Gleason6.Mean,
                           Gleason7=all.data2$Expr.Gleason7.Mean,
                           GleasonHigh=all.data2$Expr.GleasonHigh.Mean)
cat.results2 = computeCat(expr.for.cat2, idCol=1, method="equalRank", decreasing=TRUE)[[1]]$cat
catDF2=data.frame(ListSize=1:length(cat.results2), Overlap=cat.results2)

ggplot(catDF2, aes(x=ListSize, y=Overlap*100)) +
  geom_line() +
  ylim(c(0,100)) +
  labs(list(title="Overlap of most expressed tumor (by gleason score) and healthy genes", x="List size", y="Overlap")) +
  theme(axis.text.x=element_text(hjust=1),
        panel.background = element_rect(fill='#FFFFFF'),
        axis.line=element_line(colour="black", size=1, linetype="solid"),
        axis.text = element_text(colour="black", size=12),
        panel.grid.major.y = element_line(colour="#F0F0F0", size=1),
        panel.grid.minor.y = element_line(colour="#F4F4F4", size=1))
ggsave(paste0("output/TCGA/CAT plot by gleason score (all).pdf"), width=7, height=7, units="in", dpi=600)


ggplot(catDF2, aes(x=ListSize, y=Overlap*100)) +
  geom_line() +
  ylim(c(0,100)) +
  xlim(c(0,1000)) +
  labs(list(title="Overlap of most expressed tumor (by gleason score) and healthy genes", x="List size", y="Overlap")) +
  theme(axis.text.x=element_text(hjust=1),
        panel.background = element_rect(fill='#FFFFFF'),
        axis.line=element_line(colour="black", size=1, linetype="solid"),
        axis.text = element_text(colour="black", size=12),
        panel.grid.major.y = element_line(colour="#F0F0F0", size=1),
        panel.grid.minor.y = element_line(colour="#F4F4F4", size=1))
ggsave(paste0("output/TCGA/CAT plot by gleason score (1000).pdf"), width=7, height=7, units="in", dpi=600)


# Define function for region characterizations.

library(PWMEnrich)
library(BSgenome.Hsapiens.UCSC.hg19)
library(PWMEnrich.Hsapiens.background)
data(PWMLogn.hg19.MotifDb.Hsap)
library(TxDb.Hsapiens.UCSC.hg19.knownGene)

txDb <- TxDb.Hsapiens.UCSC.hg19.knownGene
orgDb <- "org.Hs.eg.db"
pwmBG = PWMLogn.hg19.MotifDb.Hsap
bsGenome = BSgenome.Hsapiens.UCSC.hg19

library(gage)
allKeggs = kegg.gsets()

#load(file.path(cache.dir, "cached.hs.keggs.RData"))

do.kegg = function(selected.genes, label, diseases=FALSE) {
  keptSets <- allKeggs$kg.sets[allKeggs$dise.idx]
  if(!diseases) {
      keptSets <- allKeggs$kg.sets[allKeggs$sigmet.idx]
  }

  allGeneIds <- expr.data$Entrez.Gene.ID

  # For all pathways, perform enrichment analysis.
  inUniverse <- as.numeric(allGeneIds)
  inDataset <- as.numeric(selected.genes)

  results <- data.frame(Pathway=character(0),
                        Chosen=numeric(0),
                        Possible=numeric(0),
                        Universe=numeric(0),
                        Drawn=numeric(0),
                        Expected=numeric(0),
                        PVal=numeric(0),
                        AdjPVal=numeric(0))
  for(kegg.set in names(keptSets)) {
    inPathway <- as.numeric(keptSets[[kegg.set]])

    chosen <- sum(unique(inDataset) %in% unique(inPathway))
    universe <- length(unique(inUniverse))
    possible <- length(unique(inPathway))
    drawn <- length(unique(inDataset))
    expected <- possible*(drawn/universe)

    # Perform the hypergeometric test.
    results <- rbind(results,
                     data.frame(Pathway=kegg.set,
                                Chosen=chosen,
                                Possible=possible,
                                Universe=universe,
                                Drawn=drawn,
                                Expected=expected,
                                PVal=phyper(chosen, possible, universe - possible, drawn, lower.tail=FALSE),
                                AdjPVal=1))
  }

  results$AdjPVal <- p.adjust(results$PVal, method="fdr")
  write.table(results[order(results$AdjPVal),], file=paste0(label, ".txt"), sep="\t", col.names=TRUE, row.names=FALSE)
}

motif.enrichment <- function(regions, file.label, use.HOCOMOCO=FALSE, top.x=20) {
    # Remove regions smaller than 30nt.
    regions.subset = regions[as.data.frame(regions)$width >= 30]

    intersectSeq <- getSeq(bsGenome, regions.subset)

    # Remove regions which have nothing but N
    intersectSeq <- intersectSeq[grepl("[ACGT]", as.data.frame(intersectSeq)[,1])]


    if(!use.HOCOMOCO) {
        res <-  motifEnrichment(intersectSeq, pwmBG)
    } else {
        res <-  motifEnrichment(intersectSeq, bg.denovo)
    }
    report <- groupReport(res)

    # Plot top X motifs
    ordered.motifs = order(res$group.bg)
    for(i in 1:top.x) {
        motif.name = gsub("Hsapiens-HOCOMOCOv9_AD_PLAINTEXT_H_PWM_hg19-", "", names(res$group.bg)[ordered.motifs[i]])
        pdf(paste(file.label, " ", i, " - ", motif.name, ".pdf"), width=7/1.5, height=11/6)
        plotMultipleMotifs(res$pwms[ordered.motifs[i]], xaxis=FALSE, titles="")
        dev.off()
    }


    write.table(as.data.frame(report), file=paste0(file.label, " MotifEnrichment.txt"),
                sep="\t", row.names=FALSE, col.names=TRUE)

    return(list(Region=regions.subset, Enrichment=res, Report=report))
}

list.txDb = as.list(txDb)
characterize.gene.set <- function(selected.genes, label) {
    out.dir = paste0("output/TCGA/", label, "/")
    dir.create(out.dir, recursive=TRUE, showWarnings=FALSE)

    write.table(all.data[all.data$Entrez %in% selected.genes,], file=paste0(out.dir, "Genes in set.txt"))

    do.kegg(selected.genes, paste0(out.dir, "KEGG.txt"))
    do.kegg(selected.genes, paste0(out.dir, "KEGG (diseases).txt"), TRUE)


    which.tx.id =  list.txDb$genes$tx_id[list.txDb$genes$gene_id %in% selected.genes]
    ranges.df = list.txDb$transcripts[which.tx.id,c("tx_chrom", "tx_start", "tx_end", "tx_strand")]
    promoter.regions = reduce(flank(GRanges(ranges.df), 1000))

    motif.enrichment(promoter.regions, out.dir, FALSE)
}

up.tumor = all.data$Entrez[all.data$Diff.Expr.Adj.PValue < 0.05 & all.data$Diff.Expr.FC > 1]
up.healthy = all.data$Entrez[all.data$Diff.Expr.Adj.PValue < 0.05 & all.data$Diff.Expr.FC < -1]

up.gleasonHigh = all.data$Entrez[all.data$Diff.Expr.Adj.PValue.H_GH < 0.05 & all.data$Diff.Expr.FC.H_GH < -1]
up.gleason6 = all.data$Entrez[all.data$Diff.Expr.Adj.PValue.H_G6 < 0.05 & all.data$Diff.Expr.FC.H_G6 < -1]
up.gleason7 = all.data$Entrez[all.data$Diff.Expr.Adj.PValue.H_G7 < 0.05 & all.data$Diff.Expr.FC.H_G7 < -1]

#Healthy vs tumor
healthy.top.1000 = (length(all.data$Expr.Healthy.Mean) - rank(all.data$Expr.Healthy.Mean)) <= 1000
tumor.top.1000 = (length(all.data$Expr.Tumor.Mean) - rank(all.data$Expr.Tumor.Mean)) <= 1000
common.core = all.data$Entrez[healthy.top.1000 & tumor.top.1000]

top.unique.healthy = all.data$Entrez[healthy.top.1000 & !tumor.top.1000]
top.unique.tumor = all.data$Entrez[!healthy.top.1000 & tumor.top.1000]

#Healthy vs gleason score
gleasonHigh.top.1000 = (length(all.data2$Expr.GleasonHigh.Mean) - rank(all.data2$Expr.GleasonHigh.Mean)) <= 1000
gleason6.top.1000 = (length(all.data2$Expr.Gleason6.Mean) - rank(all.data2$Expr.Gleason6.Mean)) <= 1000
gleason7.top.1000 = (length(all.data2$Expr.Gleason7.Mean) - rank(all.data2$Expr.Gleason7.Mean)) <= 1000
common.core2 = all.data$Entrez[gleasonHigh.top.1000 & gleason6.top.1000 & gleason7.top.1000]

top.unique.gleasonHigh = all.data2$Entrez[gleasonHigh.top.1000 & !gleason6.top.1000 & !gleason7.top.1000]
top.unique.gleason6 = all.data2$Entrez[!gleasonHigh.top.1000 & gleason6.top.1000 & !gleason7.top.1000]
top.unique.gleason7 = all.data2$Entrez[!gleasonHigh.top.1000 & !gleason6.top.1000 & gleason7.top.1000]

characterize.gene.set(up.tumor, "Upregulated in tumors")
characterize.gene.set(up.healthy, "Upregulated in healthy prostates")
characterize.gene.set(common.core, "Common core of high expressed genes")
characterize.gene.set(top.unique.healthy, "Top genes in healthy only")
characterize.gene.set(top.unique.tumor, "Top genes in tumor only")

#characterize.gene.set(up.gleasonHigh, "Upregulated in high gleason score")
#characterize.gene.set(up.gleason6, "Upregulated in gleason score 6")
#characterize.gene.set(up.gleason7, "Upregulated in gleason score 7")
characterize.gene.set(common.core2, "Common core of high expressed genes (by gleason score)")
characterize.gene.set(top.unique.gleasonHigh, "Top genes in high gleason score only")
characterize.gene.set(top.unique.gleason6, "Top genes in gleason score 6 only")
characterize.gene.set(top.unique.gleason7, "Top genes in gleason score 7 only")

# Look for motifs enriched in the common core, and remove them from the other motif sets.
table.list = list()
for(gene.set in c("Upregulated in tumors", "Upregulated in healthy prostates", "Common core of high expressed genes", "Top genes in healthy only", "Top genes in tumor only",
                  #"Upregulated in high gleason score", "Upregulated in gleason score 6", "Upregulated in gleason score 7",
                  "Common core of high expressed genes (by gleason score)",
                  "Top genes in high gleason score only", "Top genes in gleason score 6 only", "Top genes in gleason score 7 only")) {
    table.list[[gene.set]] = read.table(paste0("output/TCGA/", gene.set, "/ MotifEnrichment.txt"), sep="\t", header=TRUE, stringsAsFactors=FALSE)
    table.list[[gene.set]]$adj.p.value = p.adjust(table.list[[gene.set]]$p.value, method="fdr")
}

common.motifs = with(table.list[["Common core of high expressed genes"]], id[adj.p.value<0.01])
tumor.up.motifs = with(table.list[["Upregulated in tumors"]], id[adj.p.value<0.01])
tumor.high.motifs = with(table.list[["Top genes in tumor only"]], id[adj.p.value<0.01])
healthy.up.motifs = with(table.list[["Upregulated in healthy prostates"]], id[adj.p.value<0.01])
healthy.high.motifs = with(table.list[["Top genes in healthy only"]], id[adj.p.value<0.01])

common2.motifs = with(table.list[["Common core of high expressed genes (by gleason score)"]], id[adj.p.value<0.01])
#gleasonHigh.up.motifs = with(table.list[["Upregulated in high gleason score"]], id[adj.p.value<0.01])
gleasonHigh.high.motifs = with(table.list[["Top genes in high gleason score only"]], id[adj.p.value<0.01])
#gleason6.up.motis = with(table.list[["Upregulated in gleason score 6"]], id[adj.p.value<0.01])
gleason6.high.motifs = with(table.list[["Top genes in gleason score 6 only"]], id[adj.p.value<0.01])
#gleason7.up.motifs = with(table.list[["Upregulated in gleason score 7"]], id[adj.p.value<0.01])
gleason7.high.motifs = with(table.list[["Top genes in gleason score 7 only"]], id[adj.p.value<0.01])

write(setdiff(tumor.up.motifs, common.motifs), "output/TCGA/Tumor-specific motifs (upregulated genes).txt", sep = "\t")
write(setdiff(tumor.high.motifs, common.motifs), "output/TCGA/Tumor-specific motifs (top genes).txt", sep = "\t")
write(setdiff(healthy.up.motifs, common.motifs), "output/TCGA/Healthy prostates-specific motifs (upregulated genes).txt", sep = "\t")
write(setdiff(healthy.high.motifs, common.motifs), "output/TCGA/Healthy prostates-specific motifs (top genes).txt", sep = "\t")

write(setdiff(gleasonHigh.high.motifs, common2.motifs), "output/TCGA/High gleason score-specific motifs (top genes).txt", sep = "\t")
#write(setdiff(gleasonHigh.up.motifs, common2.motifs), "output/TCGA/High gleason score-specific motifs (upregulated genes).txt", sep = "\t")
write(setdiff(gleason6.high.motifs, common2.motifs), "output/TCGA/Gleason 6-specific motifs (top genes).txt", sep = "\t")
#write(setdiff(gleason6.up.motifs, common2.motifs), "output/TCGA/Gleason 6-specific motifs (upregulated genes).txt", sep = "\t")
write(setdiff(gleason7.high.motifs, common2.motifs), "output/TCGA/Gleason 7-specific motifs (top genes).txt", sep = "\t")
#write(setdiff(gleason7.up.motifs, common2.motifs), "output/TCGA/Gleason 7-specific motifs (upregulated genes).txt", sep = "\t")

# Generate expression plots for SIM2, FOXE1
gene.ids = c(SIM2=6493, FOXE1=2304)
expr.df.list = list()
for(i in names(gene.ids)) {
    col.index = which(colnames(healthy.data)==gene.ids[i])
    expr.df.list[[i]] <- data.frame(Expression=c(healthy.data[,col.index], 
                                                 gleason6.data[,col.index], 
                                                 gleason7.data[,col.index], 
                                                 gleasonHigh.data[,col.index]),
                                    Tissue=c(rep("Healthy", nrow(healthy.data)),
                                             rep("Gleason 6", nrow(gleason6.data)),
                                             rep("Gleason 7", nrow(gleason7.data)),
                                             rep("Gleason 8+", nrow(gleasonHigh.data))),
                                    Gene=i)
}
expr.df = do.call(rbind, expr.df.list)
expr.df$Tissue = factor(expr.df$Tissue, levels=c("Healthy", "Gleason 6", "Gleason 7","Gleason 8+"))
n.df = data.frame(table(expr.df.list[[1]]$Tissue))
n.df$Var1 = factor(n.df$Var1, levels=c("Healthy", "Gleason 6", "Gleason 7","Gleason 8+"))

ggplot(expr.df, aes(x=Tissue, y=Expression)) + 
    geom_boxplot() + 
    geom_text(data=n.df, mapping=aes(x=Var1, label=paste0("n=", Freq)), y=0, vjust=1) + 
    facet_wrap(~Gene)

ggsave("output/TCGA/FOXE1 SIM2 expression.pdf", width=7, height=7)