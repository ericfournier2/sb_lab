setwd("/home/aubag1/Projet_prostate")
dir.create("output")
dir.create("data")


if (!file.exists("data/Step-1.RData")){
  source("csaw_steps/1-Converting_reads_to_counts_mES.R")
}

if (!file.exists("data/Step-2.RData")){
  source("csaw_steps/2-Filtering_out_uninteresting_windows.R")
}

if (!file.exists("data/Step-3.RData")){
  source("csaw_steps/3-Calculating_normalization_factors.R")
}

if (!file.exists("data/results.txt")){
  source("csaw_steps/4-Testing_for_differential_binding.R")
}

if (!file.exists("data/Step-5.RData") | !file.exists("data/tabcom.txt")){
  source("csaw_steps/5-Correction_for_multiple_testing")
}

if (!file.exists("output/csaw_clusters.gz")){
  source("csaw_steps/6-Post-processing_steps.R")
}