#!/bin/bash
#PBS -l nodes=1:ppn=1
#PBS -l walltime=8:00:00
#PBS -A fhq-091-aa
#PBS -o run_csaw_mES_Med14VSControl.sh.stdout
#PBS -e run_csaw_mES_Med14VSControl.sh.stderr
#PBS -V
#PBS -N run_csaw_mES_Med14VSControl.sh

cd /mnt/parallel_scratch_mp2_wipe_on_august_2016/stbil30/aubag1/Mouse_ES

module load mugqic_dev/R_Bioconductor/3.2.4-revised_3.2
Rscript csaw_for_pipeline.R input/bam.files_Med14VSControl.txt Control mm9 ControlVSMed14_output
