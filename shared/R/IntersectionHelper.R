library(GenomicRanges)
library(VennDiagram)

source(file.path(shared, "AnnotationHelper.R"))

# Returns the set of regions within all.regions where those conditions/proteins
# at indices which.factors are present, and no other factors are. Presence/absence at
# a given locus is obtained from the overlap.matrix parameter.
exclusive.overlap.internal <- function(all.regions, overlap.matrix, which.factors) {
    has.factor = rep(TRUE, nrow(overlap.matrix))
    if(sum(which.factors) != 0) {
        has.factor = apply(overlap.matrix[,  which.factors, drop=FALSE] >= 1, 1, all)
    }
    
    no.others = rep(TRUE, nrow(overlap.matrix))
    if(sum(!which.factors) != 0) {
        no.others  = apply(overlap.matrix[, !which.factors, drop=FALSE] == 0, 1, all)
    }
    
    return(all.regions[has.factor & no.others])
}

# Performs exclusive overlap of the factors described in which.factors (logical vector of length
# intersect.object$Length) within intersect.object (as returned by build.intersect).
exclusive.overlap <- function(intersect.object, list.indices=NULL, list.names=NULL) {
    if(is.null(list.indices) && is.null(list.names)) {
       which.factors = rep(TRUE, intersect.object$Length)
    } else if (!is.null(list.names)) {
       which.factors = intersect.object$Names %in% list.names
    } else {
        which.factors = list.indices
    }
    return(exclusive.overlap.internal(intersect.object$Region, intersect.object$Matrix, which.factors))
}

# Returns the set of regions within all.regions where those conditions/proteins
# at indices which.factors are present, regardless of whether or not other factors are 
# also present at these loci. Presence/absence at a given locus is obtained from the 
# overlap.matrix parameter.
inclusive.overlap.internal <- function(all.regions, overlap.matrix, which.factors) {
    has.factor = apply(overlap.matrix[,  which.factors, drop=FALSE] >= 1, 1, all)
    return(all.regions[has.factor])
}

# Performs inclusive overlap of the factors described in which.factors (logical vector of length
# intersect.object$Length) within intersect.object (as returned by build.intersect).
inclusive.overlap <- function(intersect.object, which.factors) {
    return(inclusive.overlap.internal(intersect.object$Region, intersect.object$Matrix, which.factors))
}

# Given a GRangesList object, determine which items within the list overlap each others.
# Returns:
#   A list with the following elements:
#     Regions: A GRanges object with all genomic ranges occupied by at least one item.
#              All ranges are "flattened", so if two of the initial ranges overlapped each other
#              imperfectly, they are combined into a single region spanning both of them.
#     Matrix:  A matrix, with ncol= the number of items in the initial GRangesList and nrow=
#              the total number of loci, which is equal to the length of Regions. A value of 1 or more
#              within the matrix indicates that the regions described by the column overlapped
#              the region defined by the row.
#     List:    A list of length(grl) numeric vectors indicating which indices of Regions overlap
#              with the given condition/protein. USeful to translate the regions into unique names
#              for drawing venn diagrams.
#     Names:   The names of the initial grl items, corresponding to the column names of Matrix and the names
#              of the element of List.
#     Length:  The number of items in the initial GRangesList, corresponding to the number of columns in Matrix
#              and the number of elements in List.
build.intersect <- function(grl) {
    # Flatten the GRangesList so we can get a list of all possible regions.
    all.regions = reduce(unlist(grl))
    
    # Build a matrix to hold the results.
    overlap.matrix <- matrix(0, nrow=length(all.regions), ncol=length(grl))
    overlap.list = list()
    
    # Loop over all ranges, intersecting them with the flattened list of all possible regions.
    for(i in 1:length(grl)) {
        overlap.matrix[,i] <- countOverlaps(all.regions, grl[[i]], type="any")
        overlap.list[[ names(grl)[i] ]] <- which(overlap.matrix[,i] != 0)
    }
    colnames(overlap.matrix) <-  names(grl)
    
    return(list(Regions = all.regions, Matrix=overlap.matrix, List=overlap.list, Names=colnames(overlap.matrix), Length=ncol(overlap.matrix)))
}

# Given an intersect object returned by build.intersect, generates a venn diagram
# representing the intersections.
intersect.venn.plot <- function(intersect.object, filename) {
    venn.diagram(intersect.object$List,
                 fill=c("red", "yellow", "green", "blue", "orange")[1:intersect.object$Length],
                 filename=filename,
                 print.mode="raw")
}

# Given an intersect object returned by build.intersect, annotates the regions which are common
# to all proteins/conditions.
annotate.venn.center <- function(intersect.object, filename) {
    overlap.regions = exclusive.overlap(intersect.object, rep(TRUE, intersect.object$Length))
    
    annotate.region(overlap.regions, filename) 
}

# Given an intersect object returned by build.intersect, annotates the regions which are exclusive
# to a single proteins/conditions.
annotate.venn.exclusive <- function(intersect.object, file.prefix) {
    for(i in 1:intersect.object$Length) {
        which.factors = 1:intersect.object$Length == i
        subset.regions = exclusive.overlap(intersect.object, which.factors)
        
        if(length(subset.regions) > 0) {
            annotate.region(subset.regions, paste0(file.prefix, "Annotation for ", intersect.object$Names[i], " specific.txt"))
        }
    }
}

# Given an intersect object returned by build.intersect, calculates the pairwise overlaps
# between all proteins/conditions. Overlaps are computed both ways, with the row indicating
# the numerator.
pairwise.overlap <- function(intersect.object, filename) {
    overlap.percentage <- matrix(0, nrow=intersect.object$Length, ncol=intersect.object$Length,
                                 dimnames=list(intersect.object$Names, intersect.object$Names))
                                 
    for(i in 1:intersect.object$Length) {
        for(j in 1:intersect.object$Length) {
            overlap.percentage[i,j] = sum(intersect.object$Matrix[,i] >= 1 & intersect.object$Matrix[,j] >= 1) / sum(intersect.object$Matrix[,i] >= 1)
        }
    }
    
    write.table(overlap.percentage, file=filename, sep="\t", col.names=TRUE, row.names=TRUE, quote=FALSE)
}

# Computes the intersection between a set of regions then produces all relevant output (Venn, annotation, etc.)
build.intersect.all <- function(regions, label) {
    base.dir = file.path("output/", label)
    dir.create(base.dir, showWarnings = FALSE, recursive = TRUE)
    
    intersect.object = build.intersect(regions)

    intersect.venn.plot(intersect.object, file.path(base.dir, "Venn diagram.tiff"))
    annotate.venn.center(intersect.object, file.path(base.dir, "Venn intersection annotation.txt"))
    annotate.venn.exclusive(intersect.object, file.path(base.dir, "/"))
    pairwise.overlap(intersect.object, file.path(base.dir, "Pairwise overlap.txt"))
    
    return(intersect.object)
}
