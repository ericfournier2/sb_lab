
# Define location specific things.
ON_SERVER= Sys.info()["sysname"]!="Windows"

# Set working directory.
if (Sys.info()["nodename"] == "AUDREY-PC"){
  common.root="C:/Users/UL/Desktop/Stage/GitProstate/sb_lab"
  setwd("C:/Users/UL/Desktop/Stage/GitProstate/sb_lab/Imene_GM")
  cache.dir=file.path(common.root, "global_cache")
  shared=file.path(common.root, "shared/R")
} else if(ON_SERVER) {
    common.root="/lustre2/rap/fhq-091-aa/Working_Directory/Eric"
    setwd("/lustre2/rap/fhq-091-aa/Working_Directory/Eric/Imene_GM")
    cache.dir=file.path(common.root, "global_cache")
    shared=file.path(common.root, "shared/R")
} else {
    common.root="C:/Dev/Projects"
    setwd(file.path(common.root, "Cancer du sein"))
    cache.dir=file.path(common.root, "cache")
    shared=file.path(common.root, "shared/R")
}    

# Load ENCODExplorer to retrieve the data sets.
library(ENCODExplorer)

if(!file.exists("input/ENCODE/metadata.txt")) {
    # Plain search results (Example: searchEncode) will return datasets for which
    # bed files are not available, so we don't want to use that.
    # The results from queryEncode may contain multiple analyses for the same raw data sets,
    # some performed by the lab themselves, others performed by the ENCODE consortium.
    # By subsetting the results by lab, we only keep the ebd files generated using a unified
    # pipeline with IDR control.
    query.results = queryEncode(assay="ChIP-seq", biosample="GM12878", lab="ENCODE Consortium Analysis Working Group", file_format="bed", status="released")
    # query.results = queryEncode(assay="ChIP-seq", biosample="GM12878", file_format="bed", status="released")
    
    # query.results$experiment = subset(query.results$experiment, !grepl("control", target, ignore.case=TRUE) &
    #                                                             !is.na(target) & 
    #                                                             !grepl("^H[234]", target) & 
    #                                                             !grepl("POL", target, ignore.case = TRUE) & 
    #                                                             status != "revoked")
    
    # Download the selected results.
    downloadEncode(resultSet=query.results, resultOrigin="queryEncode", dir="input/ENCODE")
    
    # Output the metadata.
    write.table(query.results$experiment, file="input/ENCODE/metadata.txt", sep="\t", col.names=TRUE, row.names=TRUE)
    
    # Unzip the bed files.
    system("gunzip input/ENCODE/*.gz")
}

library("rtracklayer")
source(file.path(shared, "IntersectionHelper.R"))

# Read the metadata and add the full file path to it.
metadata = read.table("input/ENCODE/metadata.txt", sep="\t", header=TRUE)
metadata$path = file.path("input/ENCODE", paste0(metadata$file_accession, ".bed"))

# Load all bed files according to their accession.
extraCols_narrowPeak <- c(singnalValue = "numeric", pValue = "numeric",
                          qValue = "numeric", peak = "integer")
grl.all <- GRangesList(lapply(metadata$path, import, format="BED", extraCols=extraCols_narrowPeak))
names(grl.all) = metadata$file_accession

# Summarize groups of coordinates associated with the same protein.
list.of.granges = list()
for(protein in metadata$target) {
    file.accessions = subset(metadata, target==protein)$file_accession
    
    list.of.granges[[protein]] <- reduce(unlist(grl.all[file.accessions]))
}
grl.encode <- GRangesList(list.of.granges)


# Add our own data
peak.dir = "C:/Users/UL/Desktop/Stage/GitProstate/sb_lab/Imene_GM/input/Analyses"
#peak.dir = "/lustre2/rap/fhq-091-aa/Working_Directory/Imene/GM12878/Analyses/input"
input.peaks = list.files(peak.dir, pattern="*2WCE*")
input.df = data.frame(Path=file.path(peak.dir, input.peaks),
                      Rep=gsub("GM12878_.*_Rep(.).*", "\\1", input.peaks),
                      Factor=gsub("GM12878_(.*)_Rep.*", "\\1", input.peaks), stringsAsFactors=FALSE)
                      
grl.lab <- GRangesList(lapply(input.df$Path, import, format="BED", extraCols=extraCols_narrowPeak))                 
names(grl.lab) <- paste(input.df$Factor, input.df$Rep, sep=".")

# Summarize replicates
consensus.regions = list()
for(TF in unique(input.df$Factor)) {
    intersect.obj = build.intersect(grl.lab[input.df$Factor==TF])
    consensus.regions[[TF]] = exclusive.overlap(intersect.obj, c(TRUE, TRUE))
}
names(consensus.regions) = paste("Lab.", names(consensus.regions), sep="")
grl.lab.consensus = GRangesList(consensus.regions)

# Intersect everything
intersect.all = build.intersect(c(grl.encode, grl.lab.consensus))

# Generate sub-populations of SMC1A and NIPBL
# Generate 'SMC1A without CTCF' subpopulation.
no.ctcf = intersect.all$Matrix[,"CTCF-human"] == 0
has.smc1a = intersect.all$Matrix[,"Lab.SMC1A"] > 0
smc1a.no.ctcf = has.smc1a & no.ctcf

# Generate 'NIPBL with/without MED1' subpopulations
has.nipbl = intersect.all$Matrix[,"Lab.NIPBL"] > 0 
has.med1 = intersect.all$Matrix[,"Lab.MED1"] > 0 
nipbl.without.med1 = has.nipbl & !has.med1
nipbl.with.med1 = has.nipbl & has.med1

# Generate 'NIPBL/MED1/SMC1A' subpopulation
nipbl.smc1.med1 = has.smc1a & has.nipbl & has.med1

# SMC1A with and without NIPBL
smc1a.with.nipbl = has.smc1a & has.nipbl
smc1a.without.nipbl = has.smc1a & !has.nipbl

# Get everything into a GRangesList
grl.subgroups = GRangesList(list(SMC1A_NO_CTCF = intersect.all$Regions[smc1a.no.ctcf],
                                 SMC1A_NO_NIPBL= intersect.all$Regions[smc1a.without.nipbl],
                                 SMC1A_W_NIPBL = intersect.all$Regions[smc1a.with.nipbl],
                                 NIPBL_NO_MED1 = intersect.all$Regions[nipbl.without.med1],
                                 NIPBL_W_MED1  = intersect.all$Regions[nipbl.with.med1],
                                 NMS           = intersect.all$Regions[nipbl.smc1.med1]))

                                 
# Load bQTLs
qtl.files = list.files("input/bQTL/GWAS", pattern=".txt")
qtl.regions = list()
for(qtl.file in qtl.files) {
    region.name = paste0("QTL.", gsub(".txt", "", qtl.file))
    file.df = read.table(file.path("input/bQTL/GWAS", qtl.file), header=TRUE, sep="\t")
    region = data.frame(chr=file.df$chromosome, start=file.df$position, end=file.df$position+1)
    qtl.regions[[region.name]] = GRanges(region)
}
                                 
# Perform final intersection.
grl.all = c(grl.encode, grl.lab.consensus, grl.subgroups, GRangesList(qtl.regions))
intersect.all = build.intersect(grl.all)

# Calculate pairwise overlaps.
dir.create("output")
pairwise.overlap(intersect.all, "output/pairwise overlap.txt")

# Perform hierarchical clustering
pdf("output/Hierarchical clustering.pdf", height=7, width=14)
plot(hclust(dist(t(intersect.all$Matrix))))
dev.off()

# Compare to chromatin states
chrom.states = import("input/E116_18_core_K27ac_mnemonics.bed")

all.chrom = matrix(nrow=18, ncol=length(grl.all))
all.states = sort(unique(mcols(chrom.states)$name))

for(i in 1:length(grl.all)) {
    overlap.indices = findOverlaps(grl.all[[i]], chrom.states, select="first")
    chrom.states.all = mcols(chrom.states[overlap.indices[!is.na(overlap.indices)]])$name
    chrom.percent = table(chrom.states.all) / length(chrom.states.all)
    all.chrom[,i] = chrom.percent[match(all.states, names(chrom.percent))]
}
colnames(all.chrom) = names(grl.all)
rownames(all.chrom) = sort(unique(mcols(chrom.states)$name))

write.table(all.chrom, file="output/Chromatin states.txt", sep="\t", col.names=TRUE, row.names=TRUE)

