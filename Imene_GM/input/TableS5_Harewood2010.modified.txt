Rank in Top 11/12 list	Affymetrix ID	Gene Symbol	Chromosomal location	Mapping	Rank in C vs B list
1	231837_at	USP28	chr11q23	HSA11 to der 11	8
2	241610_x_at	PACS1	chr11q13.1	HSA11 to der 11	9
3	230306_at	VPS26B	chr11q25	HSA11 to der 22	20
4	210102_at	VWA5A	chr11q23	HSA11 to der 22	16
5	1552678_a_at	USP28	chr11q23	HSA11 to der 11	29
6	1554360_at	FCHSD2	chr11q13.4	HSA11 to der 11	70
7	244418_at	RTN3	chr11q13	HSA11 to der 11	78
8	212516_at	ARAP1	chr11q13.4	HSA11 to der 11	92
9	235010_at	LOC729013	chr11p15.3	HSA11 to der 11	105
10	226285_at	CAPRIN1	chr11p13	HSA11 to der 11	126
11	222599_s_at	NAV2	chr11p15.1	HSA11 to der 11	159
12	216171_at	EEF1G	chr11q12.3	HSA11 to der 11	188
13	222807_at	C11orf30	chr11q13.5	HSA11 to der 11	195
14	242106_at	MAPK1	chr22q11.2	HSA22 to der 11	150
15	215241_at	ANO3	chr11p14.2	HSA11 to der 11	355
16	228142_at	UQCR10	chr22cen-q12.3	HSA22 to der 11	308
17	235865_at	CELF1	chr11p11	HSA11 to der 11	264
18	218114_at	GGA1	chr22q13.31	HSA22 to der 11	335
19	635_s_at	PPP2R5B	chr11q12-q13	HSA11 to der 11	372
20	220998_s_at	UNC93B1	chr11q13	HSA11 to der 11	367
21	207180_s_at	HTATIP2	chr11p15.1	HSA11 to der 11	384
22	217185_s_at	ZNF259	chr11q23.3	HSA11 to der 11	255
23	212421_at	C22orf9	chr22q13.31	HSA22 to der 11	269
24	223172_s_at	MTP18	chr22q	HSA22 to der 11	241
25	221845_s_at	CLPB	chr11q13.4	HSA11 to der 11	389
26	227925_at	FLJ39051	chr11q24.2	HSA11 to der22	295
27	204156_at	SIK3	chr11q23.3	HSA11 to der22	533
28	208745_at	ATP5L	chr11q23.3	HSA11 to der22	438
29	208291_s_at	TH	chr11p15.5	HSA11 to der 11	621
30	229965_at	PIK4CA	chr22q11.21	HSA22 to der 11	578
31	200084_at	C11orf58	chr11p15.1	HSA11 to der 11	557
32	227086_at	HIRA	chr22q11.2	HSA22 to der 22	615
33	218327_s_at	SNAP29	chr22q11.21	HSA22 to der 11	624
34	214381_at	LOC441601	chr11p11.12	HSA11 to der 11	597
35	225744_at	ZDHHC8	chr22q11.21	HSA22 to der 22	563
36	1558613_at	OAF	chr11q23.3	HSA11 to der22	535
37	220470_at	BET1L	chr11p15.5	HSA11 to der 11	519
38	213897_s_at	MRPL23	chr11p15.5-p15.4	HSA11 to der 11	502
39	1566958_at	GAB2	chr11q14.1	HSA11 to der 11	444
40	1569022_a_at	PIK3C2A	chr11p15.5-p14	HSA11 to der 11	426
41	211725_s_at	BID	chr22q11.1	HSA22 to der 22	600
42	206580_s_at	EFEMP2	chr11q13	HSA11 to der 11	499
43	1566959_at	GAB2	chr11q14.1	HSA11 to der 11	579
44	212203_x_at	IFITM3	chr11p15.5	HSA11 to der 11	634
45	1562283_at	MAPK1	chr22q11.2	HSA22 to der 11	657
46	208810_at	DNAJB6	chr11q24.2	HSA11 to der22	679
47	222268_x_at	MUC5B	chr11p15.5	HSA11 to der 11	825
48	235062_at	LOC120379	chr11q23.1	HSA11 to der 11	779
49	1568939_at	OR8B3	chr11q24.2	HSA11 to der22	904
50	236059_at	C11orf61	chr11q24.2	HSA11 to der22	1010
